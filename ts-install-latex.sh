#!/bin/sh

set -e

REPO="${REPO:-https://github.com/latex-lsp/tree-sitter-latex.git}"
REVFILE="${REVFILE:-"${HOME}/.config/nvim/plugins/lazy/ts-nvim/parser-info/latex.revision"}"
REPONAME="tree-sitter-latex"
SOFILE="libtree-sitter-latex.so"

BUILDDIR="${BUILDDIR:-${HOME}/.local/share/nvim}"
INSTALLDIR="${INSTALLDIR:-${HOME}/.config/nvim/plugins/lazy/ts-nvim/parser}"

cd() {
    builtin cd "$1"
    echo "Changed to directory '$(pwd)'"
}

cd "$BUILDDIR"

if [ ! -d "$REPONAME" ]; then
    [ ! -d "${REPONAME}/.git" ] && rm -rf "$REPONAME"

    git clone --depth 1 "$REPO" "$REPONAME"
    pull=""
else
    pull=1
fi

cd "$REPONAME"

[ -n "$pull" ] && git pull

last_rev="$(cat "$REVFILE" || echo '')"
revision="$(git rev-parse HEAD)"

if [ "$revision" != "$last_rev" ]; then
    echo "Updating parser for 'latex'..."

    tree-sitter generate
    make

    [ ! -f "$SOFILE" ] && { echo "Error building sofile: $SOFILE" 1>&2; exit 1; }
    cp -v "$SOFILE" "${INSTALLDIR}/latex.so"

    echo "$revision" > "$REVFILE"

    echo "Parser for 'latex' updated with success"
else
    echo "Nothing to be done"
fi
