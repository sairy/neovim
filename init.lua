--                  _        --
--  _ __ ___   __ _(_)_ __   --
-- | '_ ` _ \ / _` | | '_ \  --
-- | | | | | | (_| | | | | | --
-- |_| |_| |_|\__,_|_|_| |_| --
--                           --

-- Disable these providers since we don't need them
for _, lang in ipairs { 'python3', 'ruby', 'node', 'perl' } do
    vim.g['loaded_' .. lang .. '_provider'] = 0
end

-- Additions to vim.* module
require('globals')

-- General settings
require('settings')

-- Diagnostic config
require('diagnostics')

-- Autocommands
require('autocmd')

-- Lazy setup
require('lazycfg')

-- Key bindings
require('bindings')

-- EOF
