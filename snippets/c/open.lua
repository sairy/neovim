local snip = require('luasnip')
local fmt = require('luasnip.extras.fmt').fmt
local copy = require('luasnip.extras').rep


---@param initial string
local snippet = function (initial)
    local lookup = {
        f = { 'fopen', 'fclose' },
        p = { 'popen', 'pclose' },
    }

    local funs = assert(lookup[initial], 'bad lookup value: ' .. initial)

    return snip.snippet(funs[1], fmt(
    [[
        FILE *{name};

        if (!({copy} = {open}({pathname}, {mode}))) {{
            perror("{open}: ");
            {handle}
        }}

        {END}

        {close}({copy});
    ]], {
        name = snip.insert_node(1, 'fp'),

        copy = copy(1),
        open = snip.text_node(funs[1]),
        pathname = snip.insert_node(2, 'pathname'),
        mode = snip.insert_node(3, '"[rwa]+?"'),
        handle = snip.insert_node(4, 'exit(EXIT_FAILURE);'),

        close = snip.text_node(funs[2]),

        END = snip.insert_node(0, '/* Code */')
    }))
end

for _, ft in ipairs { 'c', 'cpp' } do
    snip.add_snippets(ft, {
        snippet('f'),
        snippet('p'),
    })
end
