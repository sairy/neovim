local snip = require('luasnip')

local extra = require('luasnip.extras')
local fmt = require('luasnip.extras.fmt').fmt
local copy = extra.rep

local header_name = extra.lambda.TM_FILENAME:gsub('[%s%p-]', '_'):upper()

local snippet = snip.snippet('guard', fmt(
    [[
    #ifndef {header}
    #define {copy} 1

    {END}

    #endif /* !{copy} */
    ]], {
        header = extra.dynamic_lambda(1, header_name, {}),
        copy = copy(1),
        END = snip.insert_node(0),
    }
))

for _, ft in ipairs { 'c', 'cpp' } do
    snip.add_snippets(ft, { snippet })
end
