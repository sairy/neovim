local snip = require('luasnip')
local extras = require('luasnip.extras')
local fmt = require('luasnip.extras.fmt').fmt
local copy = extras.rep

---@param header string
---@param grep boolean?
---@param cpp boolean?
---@return string
local function include_rgx(header, grep, cpp)
    local rgxs = {
        grep = {
            '^#\\s*include\\s*<(c(?!.*\\.h))?%s(\\.h)?>$',
            '^#\\s*include\\s*<%s\\.hh\\?>$'
        },
        vim = {
            '^#\\s*include\\s*<\\(c\\(.*\\.h\\)\\@!\\)\\?%s\\(\\.h\\)\\?>$',
            '^#\\s*include\\s*<%s\\.hh\\?>$',
        }
    }

    return string.format(
        rgxs[grep and 'grep' or 'vim'][cpp and 1 or 2],
        header
    )
end

---@param lib string
---@param opts { plain: boolean, cpp: boolean, grep: boolean }?
---@return boolean
local function has_include(lib, opts)
    opts = opts or {}

    if not opts.plain then
        lib = include_rgx(lib, opts.grep, opts.cpp)
    end

    -- will only detect changes written to file
    if opts.grep then
        local bufname = vim.api.nvim_buf_get_name(0)
        vim.fn.system {
            'grep',
            '-Pm1',
            lib,
            bufname
        }
        return vim.v.shell_error == 0
    end

    local rgx = vim.regex(lib)
    for _, line in ipairs(vim.api.nvim_buf_get_lines(0, 0, -1, false)) do
        if rgx:match_str(line) then
            return true
        end
    end

    return false
end

local snippet = snip.snippet('fork', fmt(
    [[
        pid_t {var} = fork();
        switch({copy}) {{
        case -1:
            {handle1}
            {handle2}
        case 0: /* child */
            {action}
            break;
        default: /* parent */
            {END}
        }}
    ]], {
    var = snip.insert_node(1, 'pid'),
    copy = copy(1),

    handle1 = extras.partial(function ()
        return has_include('stdio', { cpp = true }) and 'perror("fork: ");' or '/* handle error */'
    end),

    handle2 = snip.dynamic_node(2, function ()
        local str = has_include('stdlib', { cpp = true }) and 'exit(EXIT_FAILURE);' or 'break;'
        return snip.sn(nil, { snip.insert_node(1, str) })
    end),

    action = snip.insert_node(3, '/* TODO */'),

    END = snip.dynamic_node(4, function (argv)
        local text = has_include('sys/wait')
            and string.format('waitpid(%s, NULL, 0);', argv[1][1])
            or ';/* TODO */'

        return snip.sn(nil, { snip.insert_node(1, text) })
    end, { 1 }),
}))


for _, ft in ipairs { 'c', 'cpp' } do
    snip.add_snippets(ft, {
        snippet,
    })
end
