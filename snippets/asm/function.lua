local snip = require('luasnip')
local fmt = require('luasnip.extras.fmt').fmt

local function copy(argv)
    return argv[1]
end

---@param local_fn boolean?
local snippet = function (local_fn)
    local t = local_fn and {
        header = [[
        .type   {fname}, @function]],
        name = 'local function',
    } or {
        header = [[
        .globl  {fname}
        .type   {copy}, @function]],
        name = 'function',
    }

    local body = [[
    {copy}:
        pushq   %rbp
        movq    %rsp, %rbp

        {END}

        popq    %rbp
        ret
    ]]

    return snip.snippet(
        t.name,
        fmt(
            string.format('%s\n%s', t.header, body), {
            fname = snip.insert_node(1, 'fn'),
            copy = snip.function_node(copy, 1),
            END = snip.insert_node(0, '## code'),
        })
    )
end

snip.add_snippets('asm', {
    snippet(true),
    snippet(),
})
