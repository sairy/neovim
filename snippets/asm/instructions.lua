local snip = require('luasnip')
local fmt = require('luasnip.extras.fmt').fmt

local function copy(argv)
    return argv[1]
end

local snippets = {
    snip.snippet('pushq', fmt(
    [[
        pushq   {reg}

        {END}

        popq    {copy}
        ]], {
            reg = snip.insert_node(1, '%rsp'),
            copy = snip.function_node(copy, 1),
            END = snip.insert_node(0, '## code')
        }
    )),
}


snip.add_snippets('asm', snippets)
