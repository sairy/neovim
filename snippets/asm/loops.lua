local snip = require('luasnip')
local fmt = require('luasnip.extras.fmt').fmt

local function copy(argv)
    return argv[1]
end

local snippets = {
    snip.snippet('for', fmt(
    [[
        pushq   {reg}

        mov{s}    {init}, {copy}
        jmp {label_end}
    {copy_loop}:
        {END}
        add{c}    {step}, {copy}
    {copy_end}:
        cmp{c}    {stop}, {copy}
        j{cond} {label_loop}

        popq    {copy}
    ]], {
            reg = snip.insert_node(1, '%rbx'),
            s = snip.insert_node(2, 'q'),
            init = snip.insert_node(3, '$0'),
            label_end = snip.insert_node(4, 'loop_end'),
            stop = snip.insert_node(5, 'STOP'),
            cond = snip.insert_node(6, 'l '),
            label_loop = snip.insert_node(7, 'loop_main'),
            step = snip.insert_node(8, '$1'),

            c = snip.function_node(copy, 2),
            copy = snip.function_node(copy, 1),
            copy_end = snip.function_node(copy, 4),
            copy_loop = snip.function_node(copy, 7),

            END = snip.insert_node(0, '## code'),
        }
    )),

    snip.snippet('while', fmt(
    [[
    {copy_loop}:
        cmp{s}    {stop}, {reg}
        j{cond} {label_end}

        {END}
        jmp {label_loop}
    {copy_end}:
    ]], {
            s = snip.insert_node(1, 'q'),
            stop = snip.insert_node(2, 'STOP'),
            reg = snip.insert_node(3, '%rcx'),
            cond = snip.insert_node(4, 'ge'),
            label_end = snip.insert_node(5, 'loop_end'),
            label_loop = snip.insert_node(6, 'loop_main'),

            copy_end = snip.function_node(copy, 5),
            copy_loop = snip.function_node(copy, 6),

            END = snip.insert_node(0, '## code'),
        }
    )),

    snip.snippet('do-while', fmt(
    [[
    {copy_loop}:
        {END}

        cmp{s}    {stop}, {reg}
        j{cond} {label_loop}
    ]], {
            s = snip.insert_node(1, 'q'),
            stop = snip.insert_node(2, 'STOP'),
            reg = snip.insert_node(3, '%rcx'),
            cond = snip.insert_node(4, 'l '),
            label_loop = snip.insert_node(5, 'loop_main'),

            copy_loop = snip.function_node(copy, 5),

            END = snip.insert_node(0, '## code'),
        }
    ))
}


snip.add_snippets('asm', snippets)
