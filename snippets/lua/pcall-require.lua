local snip = require('luasnip')
local fmt = require('luasnip.extras.fmt').fmt
local copy = require('luasnip.extras').rep

local snippet = snip.snippet('preq', fmt(
    [[
    local {bool}, {mod} = pcall(require, '{modname}')

    if not {copy} then
        {handleex}
    end

    {END}
    ]], {
        bool = snip.insert_node(1, 'ok'),
        mod = snip.insert_node(2, 'modname'),
        modname = snip.insert_node(3, 'module'),
        copy = copy(1),
        handleex = snip.insert_node(4, 'return'),
        END = snip.insert_node(0),
    }
))

snip.add_snippets('lua', { snippet })
