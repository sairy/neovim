local snip = require('luasnip')
local fmt = require('luasnip.extras.fmt').fmt
local copy = require('luasnip.extras').rep


---@param fname string
local snippet = function (fname)
    local short_name = fname:gsub('^.*%.', '')

    return snip.snippet(short_name, fmt(
    [[
    local {file} = {func}({path}, {mode})

    if not {copy} then
        {handleex}
    end

    {END}

    {copy}:close()
    ]], {
        file = snip.insert_node(1, 'f'),
        func = snip.text_node(fname),
        path = snip.insert_node(2, 'pathname'),
        mode = snip.insert_node(3, 'mode'),
        copy = copy(1),
        handleex = snip.insert_node(4, 'return'),
        END = snip.insert_node(0),
    }))
end

snip.add_snippets('lua', {
    snippet('io.open'),
    snippet('io.popen')
})
