" vim:foldmethod=marker

" Plugin calls {{{
call plug#begin('~/.config/nvim/plugins/plugged')

Plug 'wsdjeg/vim-fetch'
 	" Specify line and column with ':' when opening a file

Plug 'tpope/vim-commentary'
 	" Type gcc on a line to comment it; gc when multiple lines are selected

Plug 'jiangmiao/auto-pairs'
 	" Auto-types closing brackets, quotes, etc

Plug 'sheerun/vim-polyglot'
	" Syntax highlighting for several languages

Plug 'vlime/vlime', {'rtp': 'vim/'}
    " lisp

Plug 'uiiaoo/java-syntax.vim'
 	" Java syntax

Plug 'Yggdroot/indentLine'
 	" Show indentation lines

Plug 'alvan/vim-closetag'
 	" Close html tags

Plug 'mhinz/vim-startify'

" Plug 'norcalli/nvim-colorizer.lua'
	" Needs termguicolors set
Plug 'ap/vim-css-color'

Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'
Plug 'Rigellute/shades-of-purple.vim'
 	" Themes

Plug 'ryanoasis/vim-devicons'

call plug#end()
" }}}

