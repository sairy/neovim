--        _                              __ _        --
--  _ __ | |_   _  __ _  ___ ___  _ __  / _(_) __ _  --
-- | '_ \| | | | |/ _` |/ __/ _ \| '_ \| |_| |/ _` | --
-- | |_) | | |_| | (_| | (_| (_) | | | |  _| | (_| | --
-- | .__/|_|\__,_|\__, |\___\___/|_| |_|_| |_|\__, | --
-- |_|            |___/                       |___/  --
--                                                   --
local utils = require('util.common')

-- Startup performance
do
    local imp_ok, adhd = pcall(require, 'impatient')
    if imp_ok then
        adhd.enable_profile()
    end
end

do
    local sur_ok, surround = pcall(require, 'nvim-surround')
    if sur_ok then
        surround.setup()
    end
end

-- Treesitter
require('plugconfig.treesitter')

if not utils.istty() then
    -- Color scheme
    require('plugconfig.theme')

    -- Preview colors (e.g #FF00FF)
    require('plugconfig.colorizer')

    utils.set_guicol()
end

require('plugconfig.dressing')

require('plugconfig.telescope')

-- Comments
require('plugconfig.comments')

-- TODO highlighting
require('plugconfig.todo')

-- Indentation
require('plugconfig.indent')

-- Icons
require('plugconfig.icons')

-- Statusline
require('plugconfig.line')

-- Autopairs
require('plugconfig.autopairs')

-- Startpage
require('plugconfig.alpha')

-- Nvim-tree
require('plugconfig.nvimtree')

-- Floating terminal
require('plugconfig.fterm')

-- EOF
