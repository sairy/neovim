""      _                _             _        ""
""  ___| |__   ___  _ __| |_ ___ _   _| |_ ___  ""
"" / __| '_ \ / _ \| '__| __/ __| | | | __/ __| ""
"" \__ \ | | | (_) | |  | || (__| |_| | |_\__ \ ""
"" |___/_| |_|\___/|_|   \__\___|\__,_|\__|___/ ""
""                                              ""

" Ctrl + c/p copies to / pastes from system clipboard
vnoremap <C-c> "+y
map <C-p> "+P

vmap <C-t> d<Esc>:tabnew<CR>P

" buffers
	map <Tab> <Esc>:bnext<CR>
	map <S-Tab> <Esc>:bprevious<CR>
	map <C-d> <Esc>:bdelete<CR>

" windows
	map <C-h> <C-w>h
	map <C-j> <C-w>j
	map <C-k> <C-w>k
	map <C-l> <C-w>l

" tabs
    nmap <C-t> <Esc>:tabnew<CR>i
	" map <C-w> <Esc>:tabclose!<CR>

" TeX
	" nmap <buffer> <Leader>p :LatexPreviewToggle<CR>
	" nmap <buffer> <Leader>[ :PrevLatexPreviewMode<CR>
	" nmap <buffer> <Leader>] :NextLatexPreviewMode<CR>

"" EOF
