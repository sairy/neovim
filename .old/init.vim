let $NVIMCFG = '~/.config/nvim'

" This line makes pacman-installed global Arch Linux vim packages work.
source /usr/share/nvim/archlinux.vim

set bg=dark                      " change color scheme; either 'light' or 'dark'
filetype plugin on
highlight Folded ctermbg=238

" set termguicolors

set wrap
set linebreak

" Change bottom bar color; refer to https://jonasjacek.github.io/colors/
" hi StatusLine ctermbg=233 ctermfg=147

set incsearch                    " highlight by increments
set hlsearch                     " highlight searches

" set clipboard+=unnamedplus
set tabstop=4                    " adjust [TAB] length
set softtabstop=4
set shiftwidth=4

set expandtab                    " convert tab to space
set autoindent
set fileformat=unix              " set file format
syntax on                        " syntax highlighting
set wildmode=longest,list,full	 " word completion
set encoding=utf-8               " encoding
set number relativenumber        " add line numbers & relative numbers
set splitright                   " window splits

set foldenable

" Fix cursor not changing on exit
au VimEnter,VimResume * set guicursor=n-v-c:block-blinkon0,i-ci-ve:ver25-blinkon1,r-cr:hor20,o:hor50-blinkon1
 " \,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
 " \,sm:block-blinkwait175-blinkoff150-blinkon175

au VimLeave,VimSuspend * set guicursor=a:hor20-blinkon1

" Plugins

source $NVIMCFG/plugins.lua
source $NVIMCFG/configs.vim
" lua require'colorizer'.setup()
syntax enable

" Call airline theme
source $NVIMCFG/themes/airline.vim

" Key binindings
source $NVIMCFG/bindings.vim
