#include <stdio.h>
#include <stdlib.h>

#include <luajit-2.1/lua.h>
#include <luajit-2.1/lauxlib.h>

#include "startup.h"

static int c_startuptime(lua_State *L);
static int c_countdirents(lua_State *L);

int
c_startuptime(lua_State *L)
{
    const char *path = lua_tostring(L, -1);
    double time = startuptime(path);

    lua_pushnumber(L, time);
    return 1;
}

int
c_countdirents(lua_State *L)
{
    const char *path = lua_tostring(L, -1);
    int count = countdirents(path);

    if (count < 0) {
        lua_pushfstring(L, "count_dirents: bad directory: \"%s\"", path);
        lua_error(L);
        return 0;
    }

    lua_pushinteger(L, count);
    return 1;
}


static const luaL_Reg exports[] = {
    { "startuptime",    &c_startuptime  },
    { "countdirents",   &c_countdirents },
    { NULL, NULL }, /* sentinel */
};


int
luaopen_dashboardfuns(lua_State *L)
{
    luaL_openlib(L, "dashboardfuns", exports, 0);
    return 1;
}
