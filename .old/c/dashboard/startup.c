#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#include "startup.h"

/* used in count_dirents */
__attribute__((__always_inline__))
static inline int not_implied(const char *restrict dir)
{
    return !dir || dir[0] != '.' ||
        (dir[1] != '\0' &&
         (dir[1] != '.' || dir[2] != '\0'));
}


double
startuptime(const char *pathname)
{
    const char delim = ' ';
    char *line = NULL;
    size_t n = 0;
    ssize_t len;

    double time;
    FILE *fp;

    if (!(fp = fopen(pathname, "r")))
        return 0;

    fseek(fp, -1, SEEK_END);
    fseek(fp, fgetc(fp) == '\n' ? -2 : -1, SEEK_CUR);

    while (fgetc(fp) != '\n')
        fseek(fp, -2, SEEK_CUR);

    if ((len = getdelim(&line, &n, delim, fp)) == -1
    || line[len-1] != delim)
    {
        free(line); /* getline(3) says to do so */
        return 0;
    }

    line[len-1] = '\0';
    fclose(fp);

    if (sscanf(line, "%lf", &time) <= 0)
        time = 0; /* sscanf failed */

    free(line);
    return time;
}

int
countdirents(const char *pathname)
{
    int c;

    DIR *d;
    struct dirent *ent;

    if (!(d = opendir(pathname))) {
        return -1;
    }

    c = 0;
    while ((ent = readdir(d))) {
        if (not_implied(ent->d_name))
            c++;
    }

    closedir(d);
    return c;
}
