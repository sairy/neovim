" vim:foldmethod=marker

" Startify {{{

lua << EOF
function _G.webDevIcons(path)
    local filename = vim.fn.fnamemodify(path, ':t')
    local extension = vim.fn.fnamemodify(path, ':e')
    return require'nvim-web-devicons'.get_icon(filename, extension, { default = true })
end
EOF


function! StartifyEntryFormat() abort
    return 'v:lua.webDevIcons(absolute_path) . " " . entry_path'
endfunction


let g:ascii = [
      \ '           _            ',
      \ '  ___ ___ (_)_ __   _ _ ',
      \ '/  _   _  \ |  __)/ _  )',
      \ '| ( ) ( ) | | |  ( (_| |',
      \ '(_) (_) (_)_)_)   \__ _)',
      \ '                        ',
      \ ]
" gets messed up w/ Monaco font because of the ()'s

" let g:ascii = [
"       \ "           _           ",
"       \ " _ __ ___ (_)_ __ __ _ ",
"       \ "| '_ ` _ \\| | '__/ _` |",
"       \ "| | | | | | | | | (_| |",
"       \ "|_| |_| |_|_|_|  \\__,_|",
"       \ "                       ",
"       \ ]
" it looks scuffed here but that's because i had to add extra `\` to fix other
" characters
" and i had to use `"` to start and end because of the `'` in the middle

let g:startify_custom_header =
          \ 'startify#pad(startify#fortune#boxed() + g:ascii)'
"
" let g:startify_custom_header = startify#fortune#boxed() + g:ascii

let g:startify_lists = [
			\ { 'type': 'bookmarks',	'header': ['	Bookmarks' ]							},
			\ { 'type': 'files',		'header': ['	Recent Files']							},
			\ { 'type': 'dir',			'header': ['	Current directory ('. getcwd(). ')']	},
			\ { 'type': 'sessions',		'header': ['	Sessions']								},
			\ { 'type': 'commands',		'header': ['	Commands']								},
			\]

let g:startify_bookmarks = [ '$NVIMCFG', '$ZDOTDIR', '~/.config/kitty/kitty.conf' ]
" }}}

" Indentation plugin {{{

" Stop indentation plugin from intefering with LaTeX & markdown syntax
let g:indentLine_fileTypeExclude = ['tex', 'markdown']
let g:indentLine_char = '▏'
autocmd VimEnter * if bufname('%') == '' | IndentLinesDisable | endif

" }}}

" HTML {{{
let g:closetag_filenames = '*.html,*.xhtml,*.phtml'
" " These are the file extensions where this plugin is enabled.

let g:closetag_xhtml_filenames = '*.xhtml,*.jsx'
" " This will make the list of non-closing tags self-closing in the specified files.

let g:closetag_filetypes = 'html,xhtml,phtml'
" " These are the file types where this plugin is enabled.

let g:closetag_xhtml_filetypes = 'xhtml,jsx'
" " This will make the list of non-closing tags self-closing in the specified files.

let g:closetag_emptyTags_caseSensitive = 1
" " This will make the list of non-closing tags case-sensitive (e.g. `<Link>` will be closed while `<link>` won't.)

let g:closetag_shortcut = '>'
" " Shortcut for closing tags, default is '>'

let g:closetag_close_shortcut = '<leader>>'
" " Add > at current position without closing the current tag, default is ''
" }}}

" Auto-pairs {{{
au Filetype lisp let b:AutoPairs = {'(' : ')', '[' : ']', '{' : '}', '"' : '"', '*' : '*', '#|' : '|#'}
" }}}


"" EOF
