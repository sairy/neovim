# Requirements

- Linux/macOS
    + Configuration contains hard-coded Unix paths so it may not work on Windows
- Latest-*ish* neovim nightly
- `git`
- LSP servers / debug adapters need to be **installed separately**
    + Enabled LSP servers are specified [here](./lua/lsp/init.lua)
    + Debug adapters are specified [here](./lua/plugconfig/dap/configs/init.lua)

# Installing

If you do not wish to get rid of your existing config, Neovim allows setting the
`$NVIM_APPNAME` environment variable for deciding which configuration to use.

```bash
git clone https://gitlab.com/sairy/neovim.git ~/.config/nvim.mira
```

> **Note**
> `nvim.mira` can be whatever name you prefer, as long as it's in `~/.config/`

Then, neovim can be run the following way:
```bash
NVIM_APPNAME=nvim.mira nvim
```

or by setting the variable in your environment:
```bash
# file: ~/.profile (or anything sourced by your shell)
export NVIM_APPNAME=nvim.mira
```

# Screenshots

## Dashboard
![dashboard](./.gitlab/dashboard.png)

## Coding
![editing](./.gitlab/editing.png)

## Debugging
![debug](./.gitlab/debug.png)
## Telescope
![telescope](./.gitlab/telescope.png)

## Telescope (with transparent background)
![telescope-trans](./.gitlab/telescope-trans.png)

<!--{{{ Plugin list -->
# Plugin list

- [Comment.nvim](https://github.com/numToStr/Comment.nvim.git)
- [FTerm.nvim](https://github.com/numToStr/FTerm.nvim.git)
- [LuaSnip](https://github.com/L3MON4D3/LuaSnip.git)
- [alpha-nvim](https://github.com/goolord/alpha-nvim.git)
- [auto-pairs](https://github.com/jiangmiao/auto-pairs.git)
- [barbecue.nvim](https://github.com/utilyre/barbecue.nvim.git)
- [catppuccin.nvim](https://github.com/catppuccin/nvim.git)
- [cmp-buffer](https://github.com/hrsh7th/cmp-buffer.git)
- [cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline.git)
- [cmp-git](https://github.com/petertriho/cmp-git.git)
- [cmp-latex-symbols](https://github.com/kdheepak/cmp-latex-symbols.git)
- [cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp.git)
- [cmp-path](https://github.com/hrsh7th/cmp-path.git)
- [cmp_luasnip](https://github.com/saadparwaiz1/cmp_luasnip.git)
- [dressing.nvim](https://github.com/stevearc/dressing.nvim.git)
- [eyeliner.nvim](https://github.com/jinh0/eyeliner.nvim.git)
- [fidget.nvim](https://github.com/j-hui/fidget.nvim.git)
- [friendly-snippets](https://gitlab.com/sairy/friendly-snippets.git)
- [fugitive-gitlab.vim](https://github.com/shumphrey/fugitive-gitlab.vim.git)
- [git-blame.nvim](https://github.com/f-person/git-blame.nvim.git)
- [gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim.git)
- [guard-collection](https://github.com/nvimdev/guard-collection.git)
- [guard.nvim](https://github.com/nvimdev/guard.nvim.git)
- [headlines.nvim](https://github.com/lukas-reineke/headlines.nvim.git)
- [indentmini.nvim](https://github.com/nvimdev/indentmini.nvim.git)
- [lazy.nvim](https://github.com/folke/lazy.nvim.git)
- [lsp_lines.nvim](https://git.sr.ht/~whynothugo/lsp_lines.nvim)
- [lualine.nvim](https://github.com/nvim-lualine/lualine.nvim.git)
- [mkdir.nvim](https://github.com/jghauser/mkdir.nvim.git)
- [nvim-bqf](https://github.com/kevinhwang91/nvim-bqf.git)
- [nvim-bufdel](https://github.com/ojroques/nvim-bufdel.git)
- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp.git)
- [nvim-colorizer.lua](https://github.com/NvChad/nvim-colorizer.lua.git)
- [nvim-dap](https://github.com/mfussenegger/nvim-dap.git)
- [nvim-dap-ui](https://github.com/rcarriga/nvim-dap-ui.git)
- [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig.git)
- [nvim-navic](https://github.com/SmiteshP/nvim-navic.git)
- [nvim-notify](https://github.com/rcarriga/nvim-notify.git)
- [nvim-surround](https://github.com/kylechui/nvim-surround.git)
- [nvim-tree.lua](https://github.com/nvim-tree/nvim-tree.lua.git)
- [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter.git)
- [nvim-treesitter-textobjects](https://github.com/nvim-treesitter/nvim-treesitter-textobjects.git)
- [nvim-ts-autotag](https://github.com/windwp/nvim-ts-autotag.git)
- [nvim-web-devicons](https://github.com/nvim-tree/nvim-web-devicons.git)
- [octo.nvim](https://github.com/pwntester/octo.nvim.git)
- [plenary.nvim](https://github.com/nvim-lua/plenary.nvim.git)
- [rainbow-delimiters.nvim](https://github.com/HiPhish/rainbow-delimiters.nvim.git)
- [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim.git)
- [todo-comments.nvim](https://github.com/folke/todo-comments.nvim.git)
- [vim-dispatch](https://github.com/tpope/vim-dispatch.git)
- [vim-fugitive](https://github.com/tpope/vim-fugitive.git)
- [vim-rhubarb](https://github.com/tpope/vim-rhubarb.git)
<!--}}}-->
