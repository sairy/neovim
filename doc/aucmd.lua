---@meta
-- This is for documentation only; do not require()

---@class aucmd_callback_args
---@field id number
---@field event string
---@field group? number
---@field match string
---@field buf number
---@field file string
---@field data any

---@class aucmd_opts
---@field pattern?  string|string[]
---@field callback? string|fun(args: aucmd_callback_args):true?
---@field command?  string mutually exclusive with callback
---@field buffer?   integer
---@field group?    string|integer
---@field desc?     string
---@field once?     boolean
---@field nested?   boolean run nested aucmds


---@class augrp_opts
---@field clear boolean
