---@meta

---@class user_cmd_args
---@field name   string     Command name
---@field args?  string     The args passed to the command, if any
---@field fargs? string[]   The args split by unescaped whitespace (when more than one argument is allowed), if any
---@field nargs  string     Number of arguments
---@field bang   boolean    "true" if the command was executed with a ! modifier
---@field line1  integer    The starting line of the command range
---@field line2  integer    The final line of the command range
---@field range  integer    The number of items in the command range: 0, 1, or 2
---@field count? integer    Any count supplied
---@field reg?   string     The optional register, if specified
---@field mods?  string     Command modifiers, if any
---@field smods  table      Command modifiers in a structured format. Has the same structure as the "mods" key of `nvim_parse_cmd()`

---@class user_cmd_attr
---@field nargs?   integer|'?'|'+'|'*'
---@field desc?    string    Used for listing the command when a Lua function is used for {command}.
---@field force?   boolean   Override any previous definition.
---@field preview? function  Preview callback for 'inccommand' |:command-preview|

---Creates a global |user-commands| command.
---For Lua usage see |lua-guide-commands-create|.
---
---Example:
---```vim
--- :call nvim_create_user_command('SayHello', 'echo "Hello world!"', {'bang': v:true})
--- :SayHello
--- Hello world!
---```
---@param name    string Name of the new user command. Must begin with an uppercase letter.
---@param command string|fun(args: user_cmd_args) Replacement command to execute when this user command is executed.
---@param opts    user_cmd_attr Optional |command-attributes|.
vim.api.nvim_create_user_command = function (name, command, opts) end

---Delete a user-defined command.
---@param name string Name of the command to delete.
vim.api.nvim_buf_del_user_command = function (name) end

--[[ Buf-local ]]

---Creates a buffer-local command |user-commands|.
---@param buffer  integer Buffer handle, or 0 for current buffer.
---@param name    string  Name of the new user command. Must begin with an uppercase letter.
---@param command string|fun(args: user_cmd_args) Replacement command to execute when this user command is executed.
---@param opts    user_cmd_attr Optional |command-attributes|.
---@see vim.api.nvim_create_user_command
vim.api.nvim_buf_create_user_command = function (buffer, name, command, opts) end

---Delete a buffer-local user-defined command.
---Only commands created with |:command-buffer| or
---|nvim_buf_create_user_command()| can be deleted with this function.
---@param buffer integer  Buffer handle, or 0 for current buffer.
---@param name   string   Name of the command to delete.
vim.api.nvim_buf_del_user_command = function (buffer, name) end
