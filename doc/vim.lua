---@meta
-- This is for documentation only; do not require()

vim = require('vim.shared')

local uri = require('vim.uri')
vim.uri_decode = uri.uri_decode
vim.uri_encode = uri.uri_encode

-- let sumneko know where the sources are for the global vim runtime
-- most are already in runtime/lua/vim/_meta.lua
vim.inspect = require('vim.inspect')
