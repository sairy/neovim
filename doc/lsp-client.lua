---@meta
-- This is for documentation only; do not require()

---@class lsp_client : vim.lsp.Client
---@field initialized boolean
---@field attached_buffers boolean[]
---@field config lsp_client_cfg
---@field workspace_folders ({ name: string, uri: string })[]
---@field workspace_did_change_configuration boolean
---@field commands table<string, any>
---@field handlers table<string, fun(err: table?, result: any, ctx: table, config:table)>
local Client = {}

---@return boolean
function Client.is_stopped() end

---@class lsp_client_cfg : vim.lsp.ClientConfig
---@field cmd_env table<string,string>
---@field filetypes string[]
---@field single_file_support boolean
---@field root_dir string|fun():string
---@field settings table<string,any>
---@field commands table<string,fun()>
---@field init_options table<string,any>
---@field before_init fun(init_params: table, config: table)
---@field on_init fun(client: lsp_client, init_result: any)
---@field on_attach fun(client: lsp_client, bufnr: integer)
---@field on_error fun(err: integer, ...: any)
---@field on_exit fun(code: integer, signal: number, client_id: integer)
---@field lspinfo fun(cfg: lsp_client_cfg):string[]
---@field log_level integer
---@field workspace_folders string[]
---@field handlers table<string, fun(err: table?, result: any, ctx: table, config:table)>
---@field flags { allow_incremental_sync: boolean, debouce_text_changes: number, exit_timeout: number|boolean }
---@field __repl_opts replterm.Opts?
