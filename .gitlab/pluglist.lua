#!/usr/bin/env -S nvim -l

---@param plug LazyPluginSpec
---@return string
local function plugin_name(plug)
    local pname = plug[1] or plug.url:gsub('%.git', '')

    local n = vim.split(pname, '/')
    local name = n[#n]
    if name == 'nvim' then
        name = 'catppuccin.nvim'
    end
    return name
end

local function plug_fmt(name, url)
    return string.format('- [%s](%s)', name, url)
end

local function list_ins(t, v)
    t[#t+1] = v
    return t
end

---@type table<string,string>
local data = vim.iter(require('lazy').plugins())
    :fold({}, function (self, plug)
        self[plugin_name(plug)] = plug.url
        return self
    end)

local lines = vim.iter(vim.spairs(data))
    :map(plug_fmt)
    :fold({ '# Plugin list', '' }, list_ins)

-- scuffed but it works i guess
vim.cmd('vnew | set filetype=markdown')
local bufnr = vim.api.nvim_get_current_buf()

vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, lines)
vim.bo[bufnr].modifiable = false
vim.bo[bufnr].modified = false

vim.keymap.set('n', 'q', '<cmd>bdelete!<cr>', { buffer = bufnr })
