--           _   _   _                  --
--  ___  ___| |_| |_(_)_ __   __ _ ___  --
-- / __|/ _ \ __| __| | '_ \ / _` / __| --
-- \__ \  __/ |_| |_| | | | | (_| \__ \ --
-- |___/\___|\__|\__|_|_| |_|\__, |___/ --
--                           |___/      --

-- leader key used for keybinds
vim.g.mapleader = ' '

-- allow loading 'project-local' settings
vim.o.exrc = true

-- Mouse support
vim.o.mouse = 'nv'
vim.o.mousemodel = 'extend'

-- save undo history
vim.o.undofile = true

vim.wo.wrap = true
vim.wo.linebreak = true

-- change color scheme; either 'light' or 'dark'
-- vim.o.background = 'dark'

-- Change bottom bar color
-- vim.cmd 'hi StatusLine ctermbg=233 ctermfg=147'

-- highlight searches and do it by increments
vim.o.hlsearch = true
vim.o.incsearch = true
vim.o.ignorecase = true
vim.o.smartcase = true

-- vim.o.clipboard+=unnamedplus

-- convert [TAB] to [SPC]
vim.o.expandtab = true

-- adjust [TAB] length
vim.o.tabstop = 4      -- tabs are equal to 4 spaces
vim.o.softtabstop = 4  -- must be enabled if tabstop != 8
vim.o.shiftwidth = 4   -- use 4 spaces when indenting

-- adopt c-like indentation
vim.o.cindent = true
vim.o.cinoptions = '(0,u0,U0'

-- add line numbers
vim.wo.number = true
vim.wo.relativenumber = true
vim.wo.cursorline = true

-- window splits
vim.o.splitright = true
vim.o.splitbelow = true
vim.o.splitkeep = 'topline'

-- show tab/buffer line
vim.o.showtabline = 0

-- enable folding
vim.wo.foldenable = true
vim.wo.foldmethod = 'marker'

-- word completion
-- vim.o.wildmode = 'longest,list,full'

-- file format & encoding
vim.o.fileformat = 'unix'
vim.o.encoding = 'utf-8'

-- spellcheck
vim.o.spelllang = 'en_us,cjk'

-- default tex flavor
vim.g.tex_flavor = 'latex'

-- filetype detection and loading
vim.cmd('filetype plugin on')

-- recognize these files as markdown
vim.filetype.add {
    filename = {
        TODO = 'markdown',
        README = 'markdown',
        readme = 'markdown',
    }
}


---Colorscheme to use
---@type 'catppuccin'|'rose-pine'|'omni'
vim.g.theme = 'catppuccin'

---Which version of α-nvim to use
---@type 'dashboard'|'startify'
vim.g.alpha_dashboard = 'dashboard'

-- EOF
