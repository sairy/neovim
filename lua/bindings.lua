--  _     _           _ _                  --
-- | |__ (_)_ __   __| (_)_ __   __ _ ___  --
-- | '_ \| | '_ \ / _` | | '_ \ / _` / __| --
-- | |_) | | | | | (_| | | | | | (_| \__ \ --
-- |_.__/|_|_| |_|\__,_|_|_| |_|\__, |___/ --
--                              |___/      --
local utils = require('util.common')
local bind = utils.bind

bind('n', 'g/', vim.cmd.nohlsearch)

bind('n', 'dJ', function ()
    utils.swap_cur_line('below')
end)

bind('n', 'dK', function ()
    utils.swap_cur_line('above')
end)

bind('n', 'Q', 'gQ')
bind('n', 'X', vim.cmd.delete)

-- i always mess these up when doing things fast
for _, key in ipairs { 'W', 'Q', 'Wq', 'wQ', 'WQ' } do
    bind('c', key .. '<CR>', vim.cmd[key:lower()])
end

bind('n', '<leader>c', function ()
    vim.wo.spell = not vim.wo.spell
end)

-- move to next/prev diagnostic
bind('n', '[d', function () vim.diagnostic.jump { count = -1, float = true } end)
bind('n', ']d', function () vim.diagnostic.jump { count = 1, float = true } end)

-- Copying to/pasting from sys keyboard
bind('v',   '<C-c>',    '"+y')
bind('',    '<C-p>',    '"+p')
bind('n',   '<C-c>',    'V<C-c>', { noremap = false })

-- windows
bind('',    '<C-h>',    '<C-w>h')
bind('',    '<C-j>',    '<C-w>j')
bind('',    '<C-k>',    '<C-w>k')
bind('',    '<C-l>',    '<C-w>l')

--- move in quickfix
bind('n',   ']q',       vim.cmd.cnext)
bind('n',   '[q',       vim.cmd.cprevious)

-- buffers
bind('',    '<Tab>',    vim.cmd.bnext)
bind('',    '<S-Tab>',  vim.cmd.bprevious)

-- tabs
-- bind('n',    '<C-t>',    '<cmd>tabnew<CR>i')
-- bind('v',    '<C-x>',    'd<cmd>tabnew<CR>P')
-- bind('',     '<C-w>',    '<cmd>tabclose!<CR>')

-- terminal mode
-- bind('n',    '<leader>T','<cmd>terminal<CR>A')
-- bind('v',    '<leader>T','y<cmd>terminal<CR>$pA')
bind('t',   '<C-\\>',   '<C-\\><C-N>')

bind('n', '<leader><leader>', function ()
    local fts = { [''] = true, lua = true, vim = true }
    if fts[vim.bo.filetype] then
        vim.cmd('source')
    end
end)

-- Call shellcheck for current/chosen buffer
vim.api.nvim_create_user_command('Shellcheck', function (t)
    local buf = tonumber(t.fargs[1]) or 0

    local ftypes = { bash = true, sh = true, ksh = true, dash = true }
    if not ftypes[vim.bo[buf].filetype] then
        return
    end

    vim.cmd('!shellcheck ' .. vim.api.nvim_buf_get_name(buf))
end, { nargs = '?' })

vim.api.nvim_create_user_command('V', function (t)
    local args = #t.fargs > 0 and t.fargs or nil

    vim.cmd {
        cmd = 'G',
        args = t.fargs,
        mods = { vertical = true }
    }
end, { nargs = '?', desc = 'Shortcut for `:vert G`' })

-- EOF
