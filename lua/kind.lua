--  _           _    _           _  --
-- | |___ _ __ | | _(_)_ __   __| | --
-- | / __| '_ \| |/ / | '_ \ / _` | --
-- | \__ \ |_) |   <| | | | | (_| | --
-- |_|___/ .__/|_|\_\_|_| |_|\__,_| --
--       |_|                        --

local M = {}

local icons = {
    Array = '󰅪',
    Boolean = '◩',
    Class = '󰌗',
    Color = '󰏘',
    Constant = '󰇽',
    Constructor = '',
    Enum = '',
    EnumMember = '',
    Event = '',
    Field = '',
    File = '󰈙',
    Folder = '',
    Function = '󰊕',
    Interface = '',
    Key = '󰌋',
    Keyword = '󰌋',
    Method = 'm',
    Module = '',
    Namespace = '󰌗',
    Null = '∅',
    Number = '󰎠',
    Object = '󰅩',
    Operator = '󰆕',
    Package = '',
    Property = '',
    Reference = '',
    Snippet = '',
    String = '󰀬',
    Struct = '',
    Text = '󰉿',
    TypeParameter = '󰊄',
    Unit = '',
    Value = '󰎠',
    Variable = '󰆧',
}

local dap = {
    breakpoint = '󰝥',
    breakpoint_condition = '󰟃',
    breakpoint_rejected = '',
    disconnect = '',
    log_point = '',
    pause = '',
    play = '',
    run_last = '',
    step_back = '',
    step_into = '',
    step_out = '',
    step_over = '',
    stopped = '',
    terminate = '',
}

M = {
    icons = icons,
    dap = dap,
}

return M
