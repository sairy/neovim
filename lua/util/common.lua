--        _   _ _      --
--  _   _| |_(_) |___  --
-- | | | | __| | / __| --
-- | |_| | |_| | \__ \ --
--  \__,_|\__|_|_|___/ --
--                     --
local M = {}

---Check if neovim was launched in a TTY
---@return boolean
local istty = function ()
    return vim.env.TERM == 'linux'
end

---Sets termguicolors if they're not on
---Does nothing if in TTY
local set_guicol = function ()
    if istty() then return end

    -- 1 => enabled; 0 => disabled
    if vim.fn.getbufvar('%', '&termguicolors') ~= 1 then
        vim.o.termguicolors = true
    end
end

---Check if current dir is inside a git tree
---@return boolean
local is_gitdir = function ()
    vim.fn.system('git rev-parse --show-toplevel >/dev/null 2>&1')
    return vim.v.shell_error == 0
end

---@param specname string
---@return boolean
local hasplugin = function (specname)
    return require('lazy.core.config').plugins[specname] ~= nil
end

---Swap current line with the one below
---@param direction 'above'|'below' which line to swap with
local swap_cur_line = function (direction)
    local up = direction == 'above'

    local cur_pos = vim.api.nvim_win_get_cursor(0)
    local lineno = cur_pos[1]

    if (not up and lineno == vim.api.nvim_buf_line_count(0))
        or (up and lineno == 1)
    then
        return
    end

    local oline = lineno + (up and -1 or 1)

    local cur = vim.api.nvim_buf_get_lines(0, lineno - 1, lineno, true)
    local other = vim.api.nvim_buf_get_lines(0, oline - 1, oline, true)

    vim.api.nvim_buf_set_lines(0, lineno - 1, lineno, true, other)
    vim.api.nvim_buf_set_lines(0, oline - 1, oline, true, cur)

    vim.api.nvim_win_set_cursor(0, { oline, cur_pos[2] })
end

---@param mode keymap_mode|keymap_mode[]
---@param shortcut string sequence of keys to type
---@param action string|fun() action to perform
---@param opts? keymap_opts
local bind = function (mode, shortcut, action, opts)
    opts = vim.tbl_extend('keep', opts or {}, {
        noremap = true,
        silent = false,
    })

    return vim.keymap.set(mode, shortcut, action, opts)
end

---@param bufnr integer|boolean
---@param mode keymap_mode|keymap_mode[]
---@param shortcut string sequence of keys to type
---@param action string|fun() action to perform
---@param opts? keymap_opts
local bufbind = function (bufnr, mode, shortcut, action, opts)
    opts = opts or {}
    opts.buffer = bufnr
    return bind(mode, shortcut, action, opts)
end

---@type fun(event: string|string[], opts: aucmd_opts): number
local aucmd = vim.api.nvim_create_autocmd

---@param name string
---@param opts augrp_opts? dictionary parameters
---@return integer #augroup id
local augroup = function (name, opts)
    opts = vim.tbl_extend('keep', opts or {}, { clear = true })
    return vim.api.nvim_create_augroup(name, opts)
end

---@param name string
---@return integer #augroup id
local getaugroup = function (name)
    return augroup(name, { clear = false })
end

M = {
    istty = istty,
    set_guicol = set_guicol,

    is_gitdir = is_gitdir,
    hasplugin = hasplugin,

    swap_cur_line = swap_cur_line,

    bind = bind,
    bufbind = bufbind,

    augroup = augroup,
    aucmd = aucmd,
    getaugroup = getaugroup,
}

return M
