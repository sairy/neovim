---@meta

local notify = function (msg, level, opts)
    return vim.schedule(function ()
        vim.notify(msg, level, opts)
    end)
end

local M = {}

local LSP_NAME = 'lua_ls'

local cache = setmetatable({}, {
    __index = function (self, k)
        if k ~= 'packpath' then
            return nil
        end

        local packpath = M.real_packpath() or ''
        self[k] = packpath
        return packpath
    end,
})

---Checks if we're in neovim
---@return boolean
local function in_nvim()
    ---@diagnostic disable-next-line: param-type-mismatch
    return vim.fn.expand('%:p:h'):match(vim.fn.stdpath('config')) ~= nil
end

---Check if a path exists in the file system
---@param path string
---@return boolean
local function exists(path)
    return vim.uv.fs_stat(path) ~= nil
end

---Updates the real (non-nvim) value of package.path in a project
---@return string?
M.real_packpath = function ()
    local client = vim.lsp.get_clients {
        name = LSP_NAME,
        bufnr = 0,
    }[1]

    local lua_bin = client
        and client.config.settings.Lua.runtime.version --[[@as string]]
        or 'lua'
    lua_bin = lua_bin:gsub('%s+', ''):lower()

    ---@type string?
    local out = vim.fn.system {
        lua_bin,
        '-e',
        'pcall(require, "luarocks.loader"); io.stdout:write(package.path)',
    }

    return vim.v.shell_erorr == 0 and out or nil
end

---Searches for fname in the list of require paths
---@param ppath string
---@param fname string
---@return string?
local function incl_path(ppath, fname)
    local paths = ppath:gsub('%?', fname)

    return vim.iter(vim.gsplit(paths, '%;'))
        :find(exists)
end

---Searches for fname in the neovim runtime paths
---@param paths string[]
---@param fname string
---@param opts { sep: string?, ext: string? }
---@return string?
local function incl_rpath(paths, fname, opts)
    opts = opts or {}
    local ext = opts.ext or 'lua'
    local sep = opts.sep or (vim.fn.has('win32') == 1 and '\\' or '/')

    local modf = fname .. '.' .. ext
    local inif = 'init.' .. ext

    for _, path in ipairs(paths) do
        -- first look for lua/*.lua
        local p1 = table.concat({ path, ext, modf }, sep)
        if exists(p1) then
            return p1
        end

        -- now look for lua/*/init.lua
        local p2 = table.concat({ path, ext, fname, inif }, sep)
        if exists(p2) then
            return p2
        end
    end
end

---@return string?
local function query_lsp()
    local client = vim.lsp.get_clients {
        name = LSP_NAME,
        bufnr = 0,
    }[1]

    if not client then
        return
    end

    ---@diagnostic disable-next-line: invisible
    local reply = client:request_sync(
        'textDocument/definition',
        require('vim.lsp.util').make_position_params(0, 'utf-8'),
        nil,
        0
    )

    if not reply or reply.err then
        local err = reply and reply.err or 'No reply'
        notify(
            'Request failed: error: ' .. vim.inspect(err),
            'ERROR',
            { title = 'gf-lua' }
        )
        return
    end

    return vim.iter(reply.result or {})
        :map(function (result)
            return result.targetUri
                and vim.uri_to_fname(result.targetUri)
        end)
        :find(exists)
end

---@param modname string
---@return string? #real path
M.find_required_path = function (modname)
    local sep = vim.split(package.config, '\n')[1]

    local f
    local fname = modname:gsub('%.', sep)

    if in_nvim() then
        f = incl_path(package.path, fname) or incl_rpath(
            vim.api.nvim_list_runtime_paths(),
            fname,
            { sep = sep }
        )
    else
        f = query_lsp() or incl_path(cache.packpath, fname)
    end

    return f
end

return M
