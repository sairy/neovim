--        _                               _       --
-- __   _(_)_ __ ___  _ __ ___   ___   __| | ___  --
-- \ \ / / | '_ ` _ \| '_ ` _ \ / _ \ / _` |/ _ \ --
--  \ V /| | | | | | | | | | | | (_) | (_| |  __/ --
--   \_/ |_|_| |_| |_|_| |_| |_|\___/ \__,_|\___| --
--                                                --

local M = {}

local modes_all = {
    'normal', 'insert',
    'terminal', 'shell',
    'command', 'ex', 'more', 'confirm',
    'replace', 'v-replace',
    'visual', 'v-line', 'v-block',
    'select', 's-line', 's-block',
    'o-pending',
}

local extras = {
    'ex', 'more', 'confirm',
    'shell',
    'v-replace',
    'v-line', 'v-block',
    's-line', 's-block',
}

local modes_map = {
    normal = 'normal',
    insert = 'insert',

    terminal = 'terminal',
    command = 'command',

    replace = 'replace',

    visual = 'visual',
}
modes_map['o-pending'] = modes_map.normal

modes_map.shell = modes_map.terminal

modes_map.ex = modes_map.command
modes_map.more = modes_map.command
modes_map.confirm = modes_map.command

modes_map['v-replace'] = modes_map.replace

modes_map['v-line'] = modes_map.visual
modes_map['v-block'] = modes_map.visual

modes_map.select = modes_map.visual
modes_map['s-line'] = modes_map.select
modes_map['s-block'] = modes_map.select


local mode_icons = {
    normal = '',
    insert = '',
    terminal = '',

    command = '',

    replace = '',

    visual = '',

    select = '',
    ['o-pending'] = '',
}

for _, mode in ipairs(extras) do
    mode_icons[mode] = mode_icons[modes_map[mode]]
end

mode_icons['v-line'] = '󰉸'
mode_icons['v-block'] = '󰇀'


M = {
    all = modes_all,
    extra = extras,
    map = modes_map,
    icons = mode_icons,
}

return M
