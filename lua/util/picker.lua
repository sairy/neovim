--        _      _              --
--  _ __ (_) ___| | _____ _ __  --
-- | '_ \| |/ __| |/ / _ \ '__| --
-- | |_) | | (__|   <  __/ |    --
-- | .__/|_|\___|_|\_\___|_|    --
-- |_|                          --

---@class picker
local M = {}

---@alias picker.telescope.theme 'cursor'|'ivy'|'dropdown'

---@class picker.args
---@field prompt string?
---@field items picker.item[]
---@field config picker.config

---@class picker.config
---@field telescope { theme: fun(opts: table)|picker.telescope.theme, args: table }

---@class picker.item
---@field name string
---@field action string|fun() # vim_cmd or lua function

---@param opts picker.args
M.new = function (opts)
    local data = opts.items
    local prompt = opts.prompt
    local config = opts.config or {
        telescope = {
            theme = 'dropdown',
        },
        args = {},
    }

    return function ()
        local telescope = require('telescope.themes')
        local tfun = config.telescope.theme

        if type(tfun) == 'string' then
            if telescope['get_' .. tfun] then
                tfun = telescope['get_' .. tfun] --[[@as fun(opts: table)]]
            else
                vim.notify(
                    {
                        'Picker: Invalid telescope theme: ' .. tfun,
                        'Falling back to "dropdown theme"'
                    },
                    vim.log.levels.WARN
                )
                tfun = telescope.get_dropdown
            end
        end

        if #data == 0 then
            vim.notify('Nothing to choose from!', vim.log.levels.WARN)
            return
        end

        ---@type string[]
        local entries = vim.iter.map(function (it) return it.name end, data)

        ---@format disable-next
        vim.ui.select(
            entries,
            {
                prompt = prompt or 'Choose an action',
                telescope = tfun(config.telescope.args)
            },
            function (_, idx)
                if not idx or idx < 1 then
                    return
                end

                local chosen = data[idx]

                if type(chosen.action) == 'string' then
                    vim.cmd(chosen.action)
                elseif type(chosen.action) == 'function' then
                    vim.schedule(chosen.action--[[@as fun()]])
                else
                    vim.notify(
                    {
                        'Picker: Invalid action type!',
                        'When executing action for "' .. chosen.name .. '"',
                        'Expected string or function but found ' .. type(chosen.action),
                    },
                    vim.log.levels.ERROR
                    )
                end
            end
        )
    end
end


---for type annotations to work
---@overload fun(opts: picker.args):fun()
local _M = setmetatable(M, {
    __call = function (_, ...)
        return M.new(...)
    end,
})
---@cast _M -table
return _M
