local M = {}

---@param ts_hls { type: string[]?, typemod: string[]?, mod: string[]? }
---@return table<string,CtpHighlight>
local sem_compat = function (ts_hls)
    local t = {}

    for hltype, hls in pairs(ts_hls) do
        for _, hl in ipairs(hls) do
            local newhl = string.format('@lsp.%s.%s', hltype, hl)
            t[newhl] = { link = '@' .. hl }
        end
    end

    return t
end

M = {
    compat = sem_compat,
}

return M
