---@type LazyPluginSpec[]
local M = {
    {
        'catppuccin/nvim',
        name = 'theme.catppuccin',
        lazy = false,
        priority = 1000,
        cond = function ()
            return not require('util.common').istty()
        end,
        config = function () require('plugconfig.theme') end,
    },

    {
        'numToStr/Comment.nvim',
        name = 'comments',
        event = { 'BufReadPre', 'BufNewFile' },
        config = function () require('plugconfig.comments') end,
    },

    {
        'kylechui/nvim-surround',
        name = 'surround',
        -- pin = true,
        -- commit = 'df1c68f8fd6252a5657479aab88742f2f5f2c6b8',
        event = { 'BufReadPre', 'BufAdd', 'BufNewFile' },
        config = true,
    },

    {
        'jiangmiao/auto-pairs',
        event = 'BufWinEnter',
    },

    {
        'nvimdev/indentmini.nvim',
        name = 'indent',
        event = { 'BufReadPre', 'BufNewFile' },
        config = true,
        opts = {
            char = '│',
            exclude = {
                'alpha',
                'startify',
                'lazy',
                'packer',
                'vlime_server',
                'help',
                'terminal',
            },
            minilevel = 1,        -- number the min level that show indent line
            only_current = false, -- when true will only highlight current range
        },
    },

    {
        'ojroques/nvim-bufdel',
        name = 'bufdel',
        cmd = { 'BufDel', 'BufDelAll', 'BufDelOthers' },
        keys = {
            ---@diagnostic disable-next-line: missing-fields
            {
                '<C-f>',
                '<cmd>BufDel<cr>',
                mode = 'n',
                desc = 'Close current buffer',
            },
            ---@diagnostic disable-next-line: missing-fields
            {
                '<C-S-f>',
                '<cmd>BufDelOthers<cr>',
                mode = 'n',
                desc = 'Close every other buffer',
            },
        },
        opts = {
            next = 'tabs',
            quit = false,
        },
        config = true,
    },

    {
        'jghauser/mkdir.nvim',
        name = 'mkdir',
        event = 'BufWritePre',
    },

    {
        'jinh0/eyeliner.nvim',
        name = 'eyeliner',
        keys = { 't', 'T', 'f', 'F' },
        opts = {
            highlight_on_key = true,
            dim = true,
        },
        config = true,
    },

    {
        'HiPhish/rainbow-delimiters.nvim',
        name = 'ts.rainbow',
        event = { 'BufReadPre', 'BufAdd', 'BufNewFile' },
        dependencies = 'ts-nvim',
        config = function () require('plugconfig.treesitter.rainbow') end,
    },

    {
        'wsdjeg/vim-fetch',
        name = 'vim.fetch',
        lazy = false,
        cond = function ()
            local argv = vim.fn.argv() --[=[@as string[]]=]
            return #argv > 0 and argv[#argv]:match('.*:%d+:?%d*$')
        end,
    },

    {
        'hrsh7th/nvim-cmp',
        name = 'cmp',
        event = { 'InsertEnter', 'CmdlineEnter' },
        dependencies = {
            'luasnip',

            { 'saadparwaiz1/cmp_luasnip',   name = 'cmp.luasnip' },
            { 'hrsh7th/cmp-nvim-lsp',       name = 'cmp.nvim-lsp' },
            { 'hrsh7th/cmp-buffer',         name = 'cmp.buffer' },
            { 'hrsh7th/cmp-path',           name = 'cmp.path' },
            { 'hrsh7th/cmp-cmdline',        name = 'cmp.cmdline' },
            { 'kdheepak/cmp-latex-symbols', name = 'cmp.latex-symbols' },
            {
                'petertriho/cmp-git',
                name = 'cmp.git',
                dependencies = 'plenary',
                config = true,
            },
        },
        config = function () require('plugconfig.cmp') end,
    },

    {
        'L3MON4D3/LuaSnip',
        name = 'luasnip',
        dependencies = {
            {
                'rafamadriz/friendly-snippets',
                name = 'luasnip.friendly',
                url = 'https://gitlab.com/sairy/friendly-snippets.git',
            },
        },
        config = function () require('plugconfig.snip') end,
    },
}

return M
