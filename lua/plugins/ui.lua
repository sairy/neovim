---@type LazyPluginSpec[]
local M = {
    {
        'nvim-lualine/lualine.nvim',
        name = 'status.lualine',
        event = { 'BufReadPost', 'BufAdd', 'BufNewFile' }, --'UIEnter',
        ft = 'alpha',
        dependencies = {
            {
                'nvim-tree/nvim-web-devicons',
                name = 'devicons',
                config = function ()
                    require('plugconfig.devicons')
                end,
            },
        },
        config = function ()
            require('plugconfig.line')
        end,
    },

    {
        'akinsho/bufferline.nvim',
        name = 'status.bufferline',
        enabled = false,
        event = { 'BufReadPost', 'BufAdd', 'BufNewFile' },
        dependencies = 'devicons',
        config = function ()
            require('plugconfig.bufferline')
        end,
    },

    {
        'stevearc/dressing.nvim',
        name = 'dressing',
        event = 'VeryLazy',
        config = function ()
            require('plugconfig.dressing')
        end,
    },

    {
        'rcarriga/nvim-notify',
        name = 'ui.notify',
        init = function ()
            local builtin_notify = vim.notify

            ---@diagnostic disable-next-line: duplicate-set-field
            vim.notify = function (msg, level, opts)
                -- HACK: avoid loading nvim-notify from the
                -- parser auto-update in our treesitter config
                -- Can be changed to
                -- ```lua
                -- vim.notify = require('notify')
                -- return vim.notify(msg, level, opts)
                -- ```
                -- when/if treesitter is no longer
                -- require()'d during startup
                ---@diagnostic disable-next-line: duplicate-set-field, redefined-local
                vim.notify = function (msg, level, opts)
                    vim.notify = require('notify')
                    return vim.notify(msg, level, opts)
                end
                return builtin_notify(msg, level, opts)
            end
        end,
        opts = {
            stages = 'fade',
            -- on_close = nil,
            on_open = function (win)
                vim.api.nvim_set_option_value('winblend', 0, { scope = 'local', win = win })
                vim.api.nvim_win_set_config(win, { zindex = 90 })
            end,
            background_colour = 'NotifyBackground',
            timeout = 2000,
            fps = 20,
            -- lower levels are ignored. [ERROR > WARN > INFO > DEBUG > TRACE]
            level = 'DEBUG',

            ---@type 'default'|'minimal'|'simple'|'compact'
            render = 'compact',
            minimum_width = 50,
        },
        config = function (_, opts)
            require('notify').setup(opts)
            ---@format disable-next
            vim.api.nvim_create_user_command('NotifyTelescope', function ()
                return require('telescope').extensions.notify.notify()
            end, { nargs = 0, desc = 'Telescope UI for nvim-notify' })

            ---@format disable-next
            vim.api.nvim_create_user_command('NotifyDismiss', function ()
                return require('notify').dismiss { pending = true, silent = true }
            end, { nargs = 0, desc = 'Dismiss all notifications' })
        end,
    },

    {
        'goolord/alpha-nvim',
        name = 'alpha',
        event = 'BufWinEnter',
        config = function ()
            require('plugconfig.alpha')
        end,
    },

    {
        'NvChad/nvim-colorizer.lua',
        name = 'chad.colorizer',
        event = { 'BufReadPre', 'BufNewFile' },
        config = function ()
            require('plugconfig.colorizer')
        end,
    },

    {
        'lukas-reineke/headlines.nvim',
        name = 'headlines',
        ft = { 'markdown', 'rmd', 'org', 'norg' },
        dependencies = 'ts-nvim',
        config = function ()
            require('plugconfig.headlines')
        end,
    },

    {
        'folke/todo-comments.nvim',
        name = 'comments.todo',
        event = { 'BufReadPre', 'BufNewFile' },
        dependencies = {
            { 'nvim-lua/plenary.nvim', name = 'plenary' },
        },
        config = function ()
            require('plugconfig.todo')
        end,
    },
}

return M
