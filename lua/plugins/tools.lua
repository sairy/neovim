---@type LazyPluginSpec[]
local M = {
    {
        'nvim-telescope/telescope.nvim',
        name = 'telescope',
        cmd = 'Telescope',
        keys = {
            -- Real keys are configured in `plugconfig.telescope`
            '<leader>tf',
            '<leader>tm',
            '<leader>th',
            '<leader>tl',
            'z=',
        },
        dependencies = 'plenary',
        config = function () require('plugconfig.telescope') end,
    },

    {
        'nvim-tree/nvim-tree.lua',
        name = 'nvim-tree',
        cmd = {
            'NvimTreeFindFile',
            'NvimTreeFindFileToggle',
            'NvimTreeFocus',
            'NvimTreeOpen',
            'NvimTreeRefresh',
            'NvimTreeToggle',
        },
        keys = { '<leader>e' },
        init = function ()
            --- Open nvim-tree when running `nvim <dir_name>`
            local utils = require('util.common')
            utils.aucmd('VimEnter', {
                pattern = '*',
                callback = function (args)
                    if vim.fn.isdirectory(args.file) ~= 1 then
                        return
                    end

                    vim.api.nvim_set_current_dir(args.file)
                    require('nvim-tree.api').tree.open()
                end,
                group = utils.augroup('nvim-tree_open'),
            })
        end,
        dependencies = 'devicons',
        -- commit = '920868dba13466586897a8f40220eca6b2caac41',
        config = function () require('plugconfig.nvimtree') end,
    },

    {
        'nvimdev/guard.nvim',
        name = 'format.guard',
        event = { 'BufReadPre', 'BufAdd', 'BufNewFile' },
        dependencies = {
            { 'nvimdev/guard-collection', name = 'format.guard.presets' },
        },
        config = function () require('plugconfig.guard') end,
    },

    {
        'numToStr/FTerm.nvim',
        name = 'fterm',
        keys = {
            '<C-Space>',
            '<C-b>',
        },
        cond = function ()
            return vim.uv.os_get_passwd().uid ~= 0
        end,
        config = function () require('plugconfig.fterm') end,
    },

    {
        'kevinhwang91/nvim-bqf',
        name = 'bqf',
        ft = 'qf',
        dependencies = 'ts-nvim',
        config = function () require('plugconfig.bqf') end,
    },

    {
        'nvim-treesitter/nvim-treesitter',
        name = 'ts-nvim',
        -- lazy = false,
        -- priority = 900,
        event = 'BufReadPre',
        -- cond = function ()
        --     return vim.env['NVIM_TS_ENABLE']
        -- end,
        dependencies = {
            { 'nvim-treesitter/nvim-treesitter-textobjects', name = 'ts.textobjects' },

        },
        build = function ()
            ---@see https://github.com/nvim-treesitter/nvim-treesitter/wiki/Installation#lazynvim
            -- instead of TSUpdate
            require('nvim-treesitter.install').update { with_sync = false } ()
        end,
        config = function () require('plugconfig.treesitter') end,
    },

    {
        'windwp/nvim-ts-autotag',
        name = 'ts.autotag',
        event = { 'BufReadPre', 'BufNewFile' },
        dependencies = 'ts-nvim',
        config = true,
        opts = {
            opts = {
                enable = true,
                enable_rename = true,
                enable_close = true,
                enable_close_on_slash = true,
            },
            aliases = {},
        },
    },

    {
        'mfussenegger/nvim-dap',
        name = 'dap',
        keys = {
            '<leader>db',
            '<leader>dB',
            '<leader>dr',
            '<leader>dt',
        },
        dependencies = {
            {
                'rcarriga/nvim-dap-ui',
                name = 'dap.ui',
                config = function ()
                    require('plugconfig.dap.ui')
                end,
            },
        },
        config = function () require('plugconfig.dap') end,
    },

    {
        'tpope/vim-dispatch',
        name = 'dispatch',
        cmd = { 'Make', 'Dispatch', 'Copen', 'Focus', 'Start' },
    },

    {
        'tpope/vim-fugitive',
        name = 'git.fugitive',
        cmd = { 'Git', 'G', 'GBrowse' },
        dependencies = {
            { 'shumphrey/fugitive-gitlab.vim', name = 'git.fugitive.gitlab' },
            { 'tpope/vim-rhubarb',             name = 'git.fugitive.github' },
        },
    },

    {
        'f-person/git-blame.nvim',
        commit = '196602b570b1d754b7b8f9a9f75fa7bd88f12ef8',
        name = 'git.blame',
        event = { 'BufReadPre', 'BufNewFile' },
        cond = function ()
            return require('util.common').is_gitdir()
        end,
        config = function ()
            vim.g.gitblame_date_format = '%x %I:%M (%p)'

            -- fix conflict with cursorline
            vim.g.gitblame_set_extmark_options = {
                hl_mode = 'combine',
            }
        end,
    },

    {
        'lewis6991/gitsigns.nvim',
        name = 'git.signs',
        event = { 'CursorHold', 'CursorHoldI' },
        cond = function ()
            return require('util.common').is_gitdir()
        end,
        opts = {
            signs = {
                add = { text = '┃' },
                change = { text = '┃' },
                delete = { text = '┃' },
                topdelete = { text = '‾' },
                changedelete = { text = '~' },
                untracked = { text = '┆' },
            },
            signs_staged = {
                add = { text = '┃' },
                change = { text = '┃' },
                delete = { text = '┃' },
                topdelete = { text = '‾' },
                changedelete = { text = '~' },
                untracked = { text = '┆' },
            },

            auto_attach = true,
            signcolumn = true,
            sign_priority = 6,
            update_debounce = 100,
            word_diff = false,
            diff_opts = { internal = true },
            watch_gitdir = { follow_files = true },
            max_file_length = 40000,
            current_line_blame = false,
            -- current_line_blame_opts = {
            --     delay = 1000,
            --     virt_text = true,
            --     virtual_text_pos = 'eol',
            -- },
        },
    },

    {
        'pwntester/octo.nvim',
        name = 'git.octo',
        cmd = 'Octo',
        cond = function ()
            return require('util.common').is_gitdir()
        end,
        dependencies = {
            'plenary',
            'telescope',
            'devicons',
        },
        config = function () return require('octo').setup {} end,
    },
}

return M
