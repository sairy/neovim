---@type LazyPluginSpec[]
local M = {
    {
        'neovim/nvim-lspconfig',
        name = 'lsp',
        event = { 'BufReadPre', 'BufAdd', 'BufNewFile' },
        cond = function ()
            return vim.uv.os_get_passwd().uid ~= 0
                and not vim.env.NVIM_NOLSP
        end,
        config = function () require('lsp') end,
    },

    {
        'j-hui/fidget.nvim',
        name = 'lsp.fidget',
        pin = true,
        tag = 'legacy',
        event = 'LspAttach',
        config = function () require('lsp.fidget') end,
    },

    {
        name = 'lsp.lines',
        url = 'https://git.sr.ht/~whynothugo/lsp_lines.nvim',
        event = 'LspAttach',
        config = function ()
            local vtext = vim.diagnostic.config().virtual_text or true

            local lsp_lines = require('lsp_lines')
            lsp_lines.setup()

            local conf = {
                virtual_text = false,
                virtual_lines = true,
            }

            vim.diagnostic.config(conf)

            ---@diagnostic disable-next-line: duplicate-set-field
            lsp_lines.toggle = function ()
                local enabled = vim.diagnostic.config().virtual_lines

                if enabled then
                    vim.diagnostic.config {
                        virtual_text = vtext,
                        virtual_lines = false,
                    }
                else
                    vim.diagnostic.config(conf)
                end

                return not enabled
            end

            vim.api.nvim_create_user_command('LspLinesToggle', lsp_lines.toggle, {})
        end,
    },

    {
        'utilyre/barbecue.nvim',
        name = 'status.barbecue',
        event = { 'BufReadPost', 'BufAdd', 'BufNewFile' }, --'UIEnter',
        dependencies = 'devicons',
        config = function () require('lsp.winbar') end,
    },

    {
        'SmiteshP/nvim-navic',
        name = 'status.navic',
        event = 'LspAttach',
        config = function () require('lsp.winbar.navic') end,
    },
}

return M
