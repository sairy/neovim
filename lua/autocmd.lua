--              _                           _      --
--   __ _ _   _| |_ ___   ___ _ __ ___   __| |___  --
--  / _` | | | | __/ _ \ / __| '_ ` _ \ / _` / __| --
-- | (_| | |_| | || (_) | (__| | | | | | (_| \__ \ --
--  \__,_|\__,_|\__\___/ \___|_| |_| |_|\__,_|___/ --
--                                                 --
local utils = require('util.common')

local prefs = utils.augroup('prefs')
local highlight = utils.augroup('highlight')
local whitespace = utils.augroup('whitespace')

utils.aucmd('TextYankPost', {
    pattern = '*',
    callback = function ()
        return vim.highlight.on_yank { higroup = 'Visual', timeout = 200 }
    end,
    group = highlight,
    desc = 'Highlight yanked region',
})

utils.aucmd('BufWritePre', {
    pattern = '*',
    callback = function ()
        local curr_pos = vim.fn.getpos('.')
        vim.cmd('%s/\\s\\+$//e')
        vim.fn.cursor { curr_pos[2], curr_pos[3] }
    end,
    group = whitespace,
    desc = 'Delete trailing whitespace before saving',
})

utils.aucmd({ 'VimEnter', 'VimResume' }, {
    pattern = '*',
    callback = function ()
        vim.o.guicursor =
        'n-v-c:block-blinkon0,i-ci-ve:ver25-blinkon1,r-cr:hor20,c-o:hor50-blinkon1'
    end,
    group = prefs,
    desc = 'Change cursor while in neovim',
})

utils.aucmd('Filetype', {
    pattern = '*',
    callback = function ()
        vim.opt.formatoptions:remove('o')
    end,
    group = prefs,
    desc = 'Disable auto commenting when pressing `o`',
})

utils.aucmd({ 'TermOpen', 'TermEnter' }, {
    pattern = '*',
    callback = function ()
        vim.opt_local.number = false
        vim.opt_local.relativenumber = false
    end,
    group = prefs,
    desc = 'Disable line numbers in terminal mode',
})
