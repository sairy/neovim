--      _ _                             _   _           --
--   __| (_) __ _  __ _ _ __   ___  ___| |_(_) ___ ___  --
--  / _` | |/ _` |/ _` | '_ \ / _ \/ __| __| |/ __/ __| --
-- | (_| | | (_| | (_| | | | | (_) \__ \ |_| | (__\__ \ --
--  \__,_|_|\__,_|\__, |_| |_|\___/|___/\__|_|\___|___/ --
--                |___/                                 --

vim.diagnostic.config {
    virtual_text = {
        prefix = '●',
        source = true,
    },
    signs = {
        text = {
            [vim.diagnostic.severity.ERROR] = '',
            [vim.diagnostic.severity.WARN]  = '󰀪',
            [vim.diagnostic.severity.INFO]  = '󰌶',
            [vim.diagnostic.severity.HINT]  = '󰋽',
        },
        numhl = {
            [vim.diagnostic.severity.ERROR] = 'DiagnosticSignError',
            [vim.diagnostic.severity.WARN]  = 'DiagnosticSignWarn',
            [vim.diagnostic.severity.INFO]  = 'DiagnosticSignInfo',
            [vim.diagnostic.severity.HINT]  = 'DiagnosticSignHint',
        }
    },
    update_in_insert = false,
    underline = true,
    severity_sort = true,
    float = {
        focusable = true,
        style = 'minimal',
        border = 'rounded',
        source = true,
        header = '',
        prefix = '',
    },
}
