--  _            __  __           _ _             --
-- | |__  _   _ / _|/ _| ___ _ __| (_)_ __   ___  --
-- | '_ \| | | | |_| |_ / _ \ '__| | | '_ \ / _ \ --
-- | |_) | |_| |  _|  _|  __/ |  | | | | | |  __/ --
-- |_.__/ \__,_|_| |_|  \___|_|  |_|_|_| |_|\___| --
--                                                --

local ok, bufferline = pcall(require, 'bufferline')

if not ok then
    return
end

-- Always show tabline
vim.o.showtabline = 2

require('util.common').set_guicol()

local g = require('bufferline.groups')

-- Highlights {{{
local function gethighlights()
    local nogui = {
        underline = false,
        undercurl = false,
        italic = false,
    }

    local backup = {
        fill = {
            bg = { attribute = 'bg', highlight = 'StatusLine' },
        },
        background = {
            bg = { attribute = 'bg', highlight = 'StatusLine' },
        },
        separator = {
            bg = { attribute = 'bg', highlight = 'StatusLine' },
        },
        buffer_selected = nogui,
        diagnostic_selected = nogui,
        info_selected = nogui,
        info_diagnostic_selected = nogui,
        warning_selected = nogui,
        warning_diagnostic_selected = nogui,
        error_selected = nogui,
        error_diagnostic_selected = nogui,
        duplicate_selected = nogui,
        duplicate_visible = nogui,
        duplicate = nogui,
    }

    if (vim.g.colors_name or ''):match('catppuccin') then
        local C = require('catppuccin.palettes').get_palette() --[[@as CtpColors<string>]]
        local cat = require('catppuccin.groups.integrations.bufferline')

        local overrides = {
            styles = { 'bold' },
            custom = {
                all = {
                    fill = {
                        bg = C.crust,
                    },
                },
                [vim.g.catppuccin_flavour or 'mocha'] = {
                    background = {
                        attribute = 'fg',
                        highlight = 'Comment',
                    },
                    buffer_visible = {
                        fg = C.lavender,
                    },
                    indicator_selected = {
                        fg = C.lavender,
                    },
                },
            },
        }

        local active_bg = C.mantle
        local inactive_bg = C.crust

        -- active
        for _, section in ipairs {
            'buffer_selected',
            'duplicate_selected',
            'tab_selected',
            'tab_separator_selected',
            'indicator_selected',
            'separator_selected',
            'close_button_selected',
            'numbers_selected',
            'error_selected',
            'error_diagnostic_selected',
            'warning_selected',
            'warning_diagnostic_selected',
            'info_selected',
            'info_diagnostic_selected',
            'hint_selected',
            'hint_diagnostic_selected',
            'diagnostic_selected',
            'modified_selected',
        } do
            if not overrides.custom.all[section] then
                overrides.custom.all[section] = {}
            end
            overrides.custom.all[section].bg = active_bg
        end

        -- inactive
        for _, section in ipairs {
            'background',
            'buffer_visible',
            'duplicate_visible',
            'duplicate',
            'tab',
            'tab_close',
            'tab_separator',
            'separator',
            'separator_visible',
            'close_button',
            'close_button_visible',
            'numbers',
            'numbers_visible',
            'error',
            'error_visible',
            'error_diagnostic',
            'error_diagnostic_visible',
            'warning',
            'warning_visible',
            'warning_diagnostic',
            'warning_diagnostic_visible',
            'info',
            'info_visible',
            'info_diagnostic',
            'info_diagnostic_visible',
            'hint',
            'hint_visible',
            'hint_diagnostic',
            'hint_diagnostic_visible',
            'diagnostic',
            'diagnostic_visible',
            'modified',
        } do
            if not overrides.custom.all[section] then
                overrides.custom.all[section] = {}
            end
            overrides.custom.all[section].bg = inactive_bg
        end

        return cat.get(overrides)()
    end
    return backup
end
--}}}

bufferline.setup {
    ---@diagnostic disable-next-line: missing-fields
    options = {
        mode = 'buffers',
        always_show_bufferline = true,

        hover = {
            enabled = true,
            delay = 50,
            --reveal = { 'close' },
        },

        offsets = {
            {
                filetype = 'NvimTree',
                text = 'File Explorer',
                text_align = 'center',
                padding = 0,
            },
        },

        ---@diagnostic disable-next-line: missing-fields
        groups = {
            items = {
                g.builtin.pinned:with {
                    icon = '📌',
                },
            },
        },

        ---@param buf { name: string, path: string, bufnr: integer, buffers: integer[]?, tabnr: integer? }
        name_formatter = function (buf)
            local bo = vim.bo[buf.bufnr]
            if not bo.modifiable or bo.readonly then
                return buf.name .. ' []'
            end

            return buf.name
        end,

        custom_filter = function (buf)
            -- don't show quickfix buffers
            return vim.bo[buf].filetype ~= 'qf'
        end,

        left_mouse_command = 'buffer %d',
        right_mouse_command = 'bdelete! %d',
        middle_mouse_command = 'BufferLineTogglePin',

        show_buffer_icons = true,
        color_icons = true,

        left_trunc_marker = '',
        right_trunc_marker = '',
        modified_icon = '', --'󰏫',

        show_buffer_close_icons = true,
        buffer_close_icon = '󰅖',

        show_close_icon = true,
        close_icon = '',

        max_name_length = 14,
        max_prefix_length = 13,

        truncate_names = true,
        tab_size = 22,
        show_tab_indicators = true,
        enforce_regular_tabs = true,

        separator_style = 'thin', --slant

        diagnostics = 'nvim_lsp',
        diagnostics_update_in_insert = false,
        diagnostics_indicator = function (count)
            return '(' .. count .. ')'
        end,

        sort_by = 'insert_at_end',
        persist_buffer_sort = true,
    },

    --TODO:
    ---@diagnostic disable-next-line: assign-type-mismatch
    highlights = gethighlights(),
}
