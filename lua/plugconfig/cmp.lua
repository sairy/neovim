--                        --
--   ___ _ __ ___  _ __   --
--  / __| '_ ` _ \| '_ \  --
-- | (__| | | | | | |_) | --
--  \___|_| |_| |_| .__/  --
--                |_|     --

local snip_ok, snip = pcall(require, 'luasnip')
local cmp_ok,  cmp  = pcall(require, 'cmp')

if not snip_ok or not cmp_ok then
    return
end

vim.o.completeopt = 'menuone,noselect'

local winhighlights = 'Normal:Normal,FloatBorder:FloatBorder,CursorLine:Visual,Search:None'
local icons = require('kind').icons

local buf_opts = function ()
    local buf = vim.api.nvim_get_current_buf()
    local linecnt = vim.api.nvim_buf_line_count(buf)
    local bsize = vim.api.nvim_buf_get_offset(buf, linecnt)

    if bsize > 1024 * 1024 then
        return {}
    end

    return { buf }
end

cmp.setup {
    snippet = {
        expand = function(args)
            snip.lsp_expand(args.body)
        end,
    },

    mapping = cmp.mapping.preset.insert {
        ['<C-k>'] = cmp.mapping.select_prev_item(),
        ['<C-j>'] = cmp.mapping.select_next_item(),

        ['<C-p>'] = cmp.mapping.scroll_docs(-4),
        ['<C-n>'] = cmp.mapping.scroll_docs(4),

        ['<C-y>'] = cmp.config.disable,

        ['<C-e>'] = cmp.mapping {
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
        },

        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),

        -- Accept currently selected item.
        -- If none selected, `select` first item.
        -- Set `select` to `false` to only confirm explicitly selected items.
        ['<CR>'] = cmp.mapping.confirm {
            behavior = cmp.ConfirmBehavior.Replace,
            select = false
        },

        ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif snip.locally_jumpable(1) then
                snip.jump(1)
            else
                fallback()
            end
        end, { 'i', 's' }),

        ['<S-Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif snip.jumpable(-1) then
                snip.jump(-1)
            else
                fallback()
            end
        end, { 'i', 's' }),
    },

    formatting = {
        fields = { 'kind', 'abbr', 'menu' },
        format = function(entry, vim_item)
            local opts = {
                nvim_lsp = '[LSP]',
                luasnip  = '*snip*',
                buffer   = '[Buf]',
                path     = '[Path]',
                cmdline  = '[cmd]',

                latex_symbols = '[LaTeX]',
            }

            local ic = icons[vim_item.kind]

            if entry.source.name == 'path' and (ic == icons.File) then
                local devicons = require('nvim-web-devicons')
                vim_item.kind, vim_item.kind_hl_group = devicons.get_icon(vim_item.abbr)
                vim_item.kind = vim_item.kind or ic
            else
                vim_item.kind = ic
            end

            vim_item.menu = opts[entry.source.name]

            return vim_item
        end,
    },

    preselect = cmp.PreselectMode.None,

    sources = {
        { name = 'nvim_lsp' },
        { name = 'luasnip'  },
        { name = 'path'     },
        { name = 'git'      },
        {
            name = 'latex_symbols',
            max_item_count = 20,
        },
        {
            name = 'buffer',
            option = {
                get_bufnrs = buf_opts,
            }
        },
    },

    confirm_opts = {
        behavior = cmp.ConfirmBehavior.Replace,
        select = false,
    },

    window = {
        completion = cmp.config.window.bordered {
            winhighlight = winhighlights,
        },
        documentation = cmp.config.window.bordered {
            winhighlight = winhighlights,
        },
    },

    experimental = {
        ghost_text = true,
    }
}

cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    view = {
        entries = { name = 'custom', selection_order = 'near_cursor' },
    },
    sources = {
        { name = 'cmdline' },
        { name = 'path', option = { trailing_slash = true } },
    },
    formatting = { fields = { 'kind', 'abbr' } },
})

cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    view = {
        entries = { name = 'custom', selection_order = 'near_cursor' },
    },
    sources = {
        {
            name = 'buffer',
            option = {
                get_bufnrs = buf_opts,
            }
        }
    },
    formatting = { fields = { 'kind', 'abbr' } },
})

-- EOF
