--            _            _               --
--   ___ ___ | | ___  _ __(_)_______ _ __  --
--  / __/ _ \| |/ _ \| '__| |_  / _ \ '__| --
-- | (_| (_) | | (_) | |  | |/ /  __/ |    --
--  \___\___/|_|\___/|_|  |_/___\___|_|    --
--                                         --
local col_ok, colors = pcall(require, 'colorizer')

if not col_ok then
    return
end

local types = {
    'c',
    'conf',
    'cpp',
    'css',
    'dosini',
    'html',
    'json',
    'lisp',
    'lua',
    'markdown',
    'rasi',
    'sh',
    'svelte',
    'vim',
    'xdefaults', -- Xresources
}

local formats = {
    RGB = true,
    RRGGBB = true,
    RRGGBBAA = true,

    css = true,
    names = true,
    css_fn = true,
    tailwind = true,

    mode = 'background',
    virtualtext = '■',
}

colors.setup {
    filetypes = types,
    user_default_options = formats,
    buftypes = {},
}
