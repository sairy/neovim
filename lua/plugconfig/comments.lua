--                                           _        --
--   ___ ___  _ __ ___  _ __ ___   ___ _ __ | |_ ___  --
--  / __/ _ \| '_ ` _ \| '_ ` _ \ / _ \ '_ \| __/ __| --
-- | (_| (_) | | | | | | | | | | |  __/ | | | |_\__ \ --
--  \___\___/|_| |_| |_|_| |_| |_|\___|_| |_|\__|___/ --
--                                                    --
local cmt_ok, comments = pcall(require, 'Comment')

if not cmt_ok then
    return
end

local ft = require('Comment.ft')

comments.setup {
    padding = true,
    sticky = false,
    ignore = '^$', -- empty lines

    opleader = {
        line = 'gc',
        block = 'gb',
    },

    toggler = {
        line = 'gcc',
        block = 'gbc',
    },

    extra = {
        above = 'gcO',
        below = 'gco',
        eol = 'gcA',
    },

    mappings = {
        basic = true,
        extra = true,
    },

    pre_hook = nil,
    post_hook = nil,
}

ft.asm = { '#%s' }
ft.dosini = { '#%s' }

ft({ 'c', 'cpp' }, { '/*%s*/', '/*%s*/' })
ft({ 'lex', 'yacc' }, { '/*%s*/', '/*%s*/' })
