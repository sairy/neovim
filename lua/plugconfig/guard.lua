--                            _  --
--   __ _ _   _  __ _ _ __ __| | --
--  / _` | | | |/ _` | '__/ _` | --
-- | (_| | |_| | (_| | | | (_| | --
--  \__, |\__,_|\__,_|_|  \__,_| --
--  |___/                        --

vim.g.guard_config = {
    fmt_on_save = false,
    lsp_as_default_formatter = true,
    save_on_fmt = false,
    auto_lint = true,
    lint_interval = 500, -- how frequently can linters be called
}

local ft = require('guard.filetype')
local linters = require('guard-collection.linter')
local formatters = require('guard-collection.formatter')

local opts = {
    linters = {
        shellcheck = { 'bash', 'sh', 'zsh' },
    },
    formatters = {},
}

-- fix broken formatters/linters
do
    local sck_args = linters.shellcheck.args
    if sck_args[#sck_args] ~= '-' then
        table.insert(linters.shellcheck.args, '-')
    end
end

-- Configure formatters/linters

for linter, fts in pairs(opts.linters) do
    local ft_str = table.concat(fts, ',')

    if linters[linter] then
        ft(ft_str):lint(linter)
    end
end

for fmt, fts in pairs(opts.formatters) do
    local ft_str = table.concat(fts, ',')

    if formatters[fmt] then
        ft(ft_str):fmt(fmt)
    end
end
