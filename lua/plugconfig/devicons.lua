--      _            _                      --
--   __| | _____   _(_) ___ ___  _ __  ___  --
--  / _` |/ _ \ \ / / |/ __/ _ \| '_ \/ __| --
-- | (_| |  __/\ V /| | (_| (_) | | | \__ \ --
--  \__,_|\___| \_/ |_|\___\___/|_| |_|___/ --
--                                          --

local ok, dev = pcall(require, 'nvim-web-devicons')

if not ok then
    return
end

dev.setup {
    override = {
        alpha = {
            icon = 'α',
            color = '#b4befe',
            cterm_color = '65',
            name = 'alpha',
        },
        lisp = {
            icon = '𝝺',
            color = '#ffffff',
            cterm_color = '231',
            name = 'lisp',
        },
        asd = {
            icon = '𝝺',
            color = '#ffffff',
            cterm_color = '231',
            name = 'asd',
        },
        PKGBUILD = {
            icon = '',
            color = '#00d7ff',
            cterm_color = '45',
            name = 'PKGBUILD',
        },
        zsh = {
            icon = '',
            color = '#fab387',
            cterm_color = '65',
            name = 'zsh',
        },
        vim = {
            icon = '',
            color = '#a6e3a1',
            cterm_color = '65',
            name = 'vim',
        },
    },

    color_icons = true,
    default = false,
    strict = false,
}

-- Change default icon
dev.get_default_icon().icon = ''

-- Override get_icon_by_filetype()
local get_icon_by_ft = dev.get_icon_by_filetype

---@param ft string
---@param opts { default: boolean, strict: boolean }
---@return string?, string?
---@diagnostic disable-next-line: duplicate-set-field
dev.get_icon_by_filetype = function (ft, opts)
    local icon, hl = get_icon_by_ft(ft, opts)

    if icon == nil then
        icon, hl = dev.get_icon(ft, nil, {})
    end

    return icon, hl
end
