--  _            _        --
-- | |_ ___   __| | ___   --
-- | __/ _ \ / _` |/ _ \  --
-- | || (_) | (_| | (_) | --
--  \__\___/ \__,_|\___/  --
--                        --
local ok, todo = pcall(require, 'todo-comments.init')

if not ok then
    return
end

todo.setup {
    signs = true,
    sign_priority = 8,

    keywords = {
        FIX = {
            icon = '', -- icon used for the sign, and in search results
            color = 'error',
            alt = { 'FIXME', 'BUG', 'FIXIT', 'ISSUE' }, --aliases
        },
        TODO = { icon = '', color = 'info' },
        HACK = { icon = '', color = 'warning' },
        WARN = { icon = '', color = 'warning', alt = { 'WARNING', 'XXX' } },
        PERF = { icon = '󰅒', alt = { 'OPTIM', 'PERFORMANCE', 'OPTIMIZE' } },
        NOTE = { icon = '󰍨', color = 'hint', alt = { 'INFO' } },
        TEST = {
            icon = '⏲',
            color = 'test',
            alt = { 'TESTING', 'PASSED', 'FAILED' }
        },
    },

    gui_style = {
        fg = 'NONE',
        bg = 'BOLD',
    },

    merge_keywords = true,

    -- highlighting of the line containing the todo comment
    -- * before: highlights before the keyword (typically comment characters)
    -- * keyword: highlights of the keyword
    -- * after: highlights after the keyword (todo text)
    highlight = {
        multiline = true,
        multiline_pattern = '^.',
        multiline_context = 10,
        before = 'fg',
        keyword = 'wide_bg',
        after = 'fg',
        pattern = [[.*<(KEYWORDS)\s*:?]],
        comments_only = true,
        max_line_len = 400,
        exclude = {
            'alpha',
            'lazy',
            'startify',
            'packer',
            'vlime_server',
            'help',
            'terminal'
        },
    },

    search = {
        command = 'rg',
        args = {
            '--color=never',
            '--no-heading',
            '--with-filename',
            '--line-number',
            '--column',
            '--engine=default',
        },
        pattern = [[\b(KEYWORDS):]],
    },

    -- list of named colors where we try to extract the guifg from the
    -- list of highlight groups or use the hex color if hl not found as a fallback
    colors = {
        error = { 'DiagnosticError', 'ErrorMsg', '#DC2626' },
        warning = { 'DiagnosticWarning', 'WarningMsg', '#FBBF24' },
        info = { 'DiagnosticInfo', '#2563EB' },
        hint = { 'DiagnosticHint', '#10B981' },
        default = { 'Identifier', '#7C3AED' },
        test = { 'Identifier', '#FF00FF' }
    },
}
