local ok, headlines = pcall(require, 'headlines')

if not ok then
    return
end

headlines.setup {
    markdown = {
        headline_highlights = {
            'Headline1',
            'Headline2',
            'Headline3',
            'Headline4',
            'Headline5',
            'Headline6',
        },
        fat_headlines = true,
        fat_headline_upper_string = '▃',
        fat_headline_lower_string = '🬂',
    }
}
