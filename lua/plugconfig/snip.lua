--            _                  _        --
--  ___ _ __ (_)_ __  _ __   ___| |_ ___  --
-- / __| '_ \| | '_ \| '_ \ / _ \ __/ __| --
-- \__ \ | | | | |_) | |_) |  __/ |_\__ \ --
-- |___/_| |_|_| .__/| .__/ \___|\__|___/ --
--             |_|   |_|                  --

local ok, snip = pcall(require, 'luasnip')

if not ok then
    return
end

snip.config.set_config {
    history = false,

    update_events = { 'TextChanged', 'TextChangedI' },

    region_check_events = 'CursorMoved',
    delete_check_events = 'TextChanged',
}

-- Include doc snippets
snip.filetype_extend('c', { 'cdoc' })
snip.filetype_extend('cpp', { 'cppdoc' })
snip.filetype_extend('csharp', { 'csharpdoc' })
snip.filetype_extend('java', { 'javadoc' })
snip.filetype_extend('javascript', { 'jsdoc' })
snip.filetype_extend('kotlin', { 'kdoc' })
snip.filetype_extend('php', { 'phpdoc' })
snip.filetype_extend('rust', { 'rustdoc' })
snip.filetype_extend('shell', { 'shelldoc' })
snip.filetype_extend('typescript', { 'tsdoc' })

require('luasnip.loaders.from_vscode').lazy_load()

vim.schedule(function ()
    ---@type string[]
    local snippets = vim.fn.glob(
        vim.fn.stdpath('config') .. '/snippets/**/*.lua',
        false,
        true
    )

    for _, file in ipairs(snippets) do
        vim.schedule(function ()
            local fun = loadfile(file)
            if fun then fun() end
        end)
    end
end)

vim.api.nvim_create_user_command(
    'LuaSnipEdit',
    function (args)
        require('luasnip.loaders').edit_snippet_files {
            ft_filter = #args.fargs > 0 and function (ft)
                return vim.list_contains(args.fargs, ft)
            end or nil,
            edit = vim.cmd.tabedit,
        }
    end,
    { nargs = '*', desc = 'Edit Snippets', complete = 'filetype' }
)
