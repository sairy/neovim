--  _             _ _             --
-- | |_   _  __ _| (_)_ __   ___  --
-- | | | | |/ _` | | | '_ \ / _ \ --
-- | | |_| | (_| | | | | | |  __/ --
-- |_|\__,_|\__,_|_|_|_| |_|\___| --
--                                --
local ok, line = pcall(require, 'lualine')

if not ok then
    return
end

-- Messages like --INSERT-- are no longer needed
vim.o.showmode = false

---@alias section_color { bg: string, fg: string, gui: string? }
---@alias section { ['a'|'b'|'c']: section_color }
---@alias lualine_theme string|{ ['normal'|'insert'|'command'|'visual'|'replace'|'inactive']: section }

local lualine_util = require('lualine.utils.mode')
local util = require('util.common')
local vimmode = require('util.vimmode')


---Checks whether a component should be shortened due to winlen
---@param str? string new component text
---@param alt string|fun():string alternative text
---@return string?
local function maybe_shorten(str, alt)
    --- output of `vim.fn.winwidth(0)` w/ my dwm
    --- when neovim is in the master window
    local masterwidth = 103

    if vim.fn.winwidth(0) <= masterwidth then
        return type(alt) == 'string' and alt or alt()
    end

    return str
end

local function repo_name()
    ---@type string
    local fp = vim.fn.system { 'git', 'rev-parse', '--show-toplevel' }
    return vim.fn.fnamemodify(fp:gsub('\n', ''), ':t') or 'git'
end

---@return lualine_theme
local function get_theme()
    local fallback = 'dracula'

    local theme_name = vim.g.colors_name or ''

    if theme_name:match('catppuccin') then
        local C = require('catppuccin.palettes').get_palette() --[[@as CtpColors<string>]]
        local cp = require('lualine.themes.catppuccin')

        cp.normal.a.bg = C.lavender
        cp.normal.b.fg = C.lavender
        cp.inactive.a.fg = C.lavender

        cp.normal.c.bg = C.crust

        for section in pairs(cp.inactive) do
            cp.inactive[section].bg = C.mantle
        end

        return cp
    elseif theme_name == 'rose-pine' then
        local rp = require('lualine.themes.rose-pine')

        -- add custom section for terminal mode
        if not rp.terminal then
            rp.terminal = rp.command
        end

        for mode in pairs(rp) do
            rp[mode].c.bg = '#1a1828'
        end
        return rp
    else
        return fallback
    end
end


-- Theme to use
local ll_theme = get_theme()

---@return string
local function get_mode()
    local ll_mode = lualine_util.get_mode()

    local mapped = vimmode.map[ll_mode:lower()]

    if not mapped then
        vim.notify(
            'lualine: unknown mapping: ' .. ll_mode,
            vim.log.levels.WARN
        )
        return 'normal'
    end

    return mapped
end

local fname, tabline
-- No bufferline {{{
if not util.hasplugin('status.bufferline') then
    local tabline_coloring = {
        active = function ()
            local mode = get_mode()

            if type(ll_theme) ~= 'table' or not ll_theme[mode] then
                vim.notify('buffers_color: unknown mode ' .. tostring(mode))
                return 'lualine_a_insert'
            end

            return {
                bg = ll_theme[mode].a.bg,
                fg = ll_theme[mode].a.fg,
                gui = ll_theme[mode].a.gui,
            }
        end,

        inactive = function ()
            local mode = get_mode()

            if type(ll_theme) ~= 'table' or not ll_theme[mode] then
                vim.notify('buffers_color: unknown mode ' .. tostring(mode))
                return 'lualine_a_inactive'
            end

            return {
                bg = ll_theme.inactive.a.bg,
                fg = ll_theme[mode].a.bg,
                gui = ll_theme.inactive.a.gui,
            }
        end,
    }

    fname = {
        'filename',

        ---@param name string
        fmt = function (name)
            if vim.bo.filetype == 'alpha' then
                return '[]'
            end

            local maxlen = 30
            local fallback = vim.fn.expand('%:t') --[[@as string]]

            if #name > maxlen then
                return maybe_shorten(name, fallback)
            end

            local root = vim.lsp.buf.list_workspace_folders()[1] --[[@as string?]]

            if not root then
                return name
            elseif root:sub(#root) ~= '/' then
                root = root .. '/'
            end

            local bufname = vim.api.nvim_buf_get_name(0):gsub(root, '')

            return #bufname <= maxlen
                and bufname
                or maybe_shorten(bufname, fallback)
        end,

        path = 1,
        file_status = true,
        newfile_status = true,

        shorting_target = 40,

        symbols = {
            modified = ' [+]',
            readonly = ' []', -- f023
            unnamed = '[名無し]',
            newfile = '[New]',
        },
    }

    tabline = {
        lualine_a = {
            {
                'buffers',
                show_filename_only = true,
                show_modified_status = true,
                mode = 0,

                filetype_names = {
                    alpha = 'Alpha',
                    lazy = 'Lazy',
                    startify = 'Startify',
                    packer = 'Packer',
                    vlime_server = 'REPL',
                    NvimTree = 'NvimTree',
                },

                buffers_color = tabline_coloring,

                symbols = {
                    modified = ' ●',
                    alternate_file = '#',
                    directory = '',
                },
            },
        },

        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},

        lualine_z = {
            {
                'tabs',
                mode = 0,

                tabs_color = tabline_coloring,
            },
        },
    }
    fname = nil
    tabline = nil
end
-- }}}

local function comp_seps()
    if vim.uv.os_getenv('TERM') == 'xterm-kitty' then
        return { left = '', right = '' } -- eb01 | eb03
    end

    return { left = '', right = '' } -- e0b5 | e0b7
end

line.setup {
    options = {
        icons_enabled = true,
        theme = ll_theme,
        component_separators = comp_seps(),
        section_separators = {
            left = '', -- e0b6
            right = '', -- e0b4
        },

        always_divide_middle = true,
        globalstatus = true,

        refresh = {
            statusline = 1000,
            tabline = 1000,
            winbar = 1000,
        },

        ignore_focus = {},
        disabled_filetypes = {},
    },

    sections = {
        lualine_a = {
            {
                'mode',
                ---@param mode string
                fmt = function (mode)
                    local icon = vimmode.icons[mode:lower()]
                    if not icon then
                        vim.notify(
                            string.format('Invalid icon for mode: "%s"', mode),
                            vim.log.levels.ERROR
                        )
                        return mode
                    end

                    return string.format('%s %s', icon, mode)
                end,
                padding = {
                    left = 0,
                    right = 1,
                },
                separator = {
                    left = '',
                    right = '',
                },
            },
        },

        lualine_b = {
            {
                'branch',
                icon = '', -- f126

                ---@param branch string
                fmt = function (branch)
                    local maxlen = 20
                    if #branch > maxlen then
                        return maybe_shorten(branch, repo_name)
                    end

                    return branch
                end,
            },

            {
                'diff',

                symbols = {
                    added = ' ',  -- f457
                    modified = ' ', -- f459
                    removed = ' ', -- f458
                },

                colored = true,
                separator = '|',
            },

            {
                'diagnostics',
                sources = { 'nvim_lsp' },

                sections = { 'hint', 'info', 'warn', 'error' },

                always_visible = false,
                update_in_insert = false,

                symbols = {
                    error = ' ', -- f468
                    warn = '󰀪 ', -- f529
                    hint = '󰌶 ', -- f835
                    info = '󰋽 ', -- f7fc
                },

                colored = true,
            },
        },

        lualine_c = { fname },

        lualine_x = {},

        lualine_y = {
            {
                'filetype',
                colored = true,
                icon_only = false,
            },

            {
                'encoding',
                ---@param enc string
                fmt = function (enc)
                    return enc:lower()
                end,
                padding = { left = 1, right = 0 },
                separator = '',
            },

            {
                'fileformat',
                symbols = {
                    unix = '', -- f17c
                    dos = '', -- e62a
                    mac = '', -- f179
                },
                padding = { left = 1, right = 1 },
            },
        },

        lualine_z = {
            {
                'progress',
                ---@param str string
                fmt = function (str)
                    local icon = ''

                    if str == '0%' then
                        str = 'Top'
                    elseif str == '100%' then
                        str = 'Bot'
                    end

                    return string.format('%s %s', icon, str)
                end,

                padding = { left = 1, right = 1 },
            },

            {
                'location',
                ---@param str string
                fmt = function (str)
                    return string.format('%s %s', '', str:gsub('%s+', ''))
                end,
                separator = {
                    right = '',
                },
                padding = {
                    left = 1,
                    right = 0,
                },
            },
        },
    },

    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = { 'filename' },
        lualine_x = { 'location' },
        lualine_y = {},
        lualine_z = {},
    },

    tabline = tabline,

    winbar = {},
    inactive_winbar = {},

    extensions = {
        'fugitive',
        'lazy',
        'man',
        'nvim-dap-ui',
        'nvim-tree',
        'quickfix',
    },
}
