local ok, dapui = pcall(require, 'dapui')

if not ok then
    return
end

local dap = require('dap')
local icons = require('kind')

dapui.setup {
    icons = {
        expanded = '',
        collapsed = '',
        current_frame = '',
    },

    element_mappings = {},
    mappings = {
        expand = '<CR>',
        open = 'o',
        remove = 'd',
        edit = 'e',
        repl = 'r',
        toggle = 't',
    },

    layouts = {
        {
            elements = {
                { id = 'scopes',      size = 0.30 },
                { id = 'watches',     size = 0.30 },
                { id = 'stacks',      size = 0.30 },
                { id = 'breakpoints', size = 0.10 },
            },
            size = 40,
            position = 'left',
        },
        { elements = { 'repl' }, size = 10, position = 'bottom' },
    },

    controls = {
        enabled = true,
        element = 'repl',
        icons = icons.dap,
    },

    expand_lines = true,
    force_buffers = true,

    render = {
        indent = 1,
    },

    floating = {
        max_height = nil,
        max_width = nil,
        border = 'rounded',
        mappings = { close = { 'q', '<Esc>' } },
    },

    windows = { indent = 1 },
}

local listeners = {
    event_initialized = dapui.open,
    event_terminated = dapui.close,
    event_exited = dapui.close,
}

for event, handle in pairs(listeners) do
    dap.listeners.after[event].dapui_config = handle
end

vim.api.nvim_create_user_command('DapUIOpen', function ()
    return dapui.open { reset = true }
end, { desc = 'Open DAP UI', nargs = 0 })

vim.api.nvim_create_user_command('DapUIClose', function ()
    return dapui.close()
end, { desc = 'Close DAP UI', nargs = 0 })

vim.api.nvim_create_user_command('DapUIToggle', function ()
    return dapui.toggle { reset = false }
end, { desc = 'Toggle DAP UI', nargs = 0 })
