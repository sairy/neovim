local dap = require('dap')

---@param fun fun(done_cb: fun(...: any))
---@return fun():thread
local async = function (fun)
    return function ()
        return coroutine.create(function (dap_run_co)
            fun(function (...)
                coroutine.resume(dap_run_co, ...)
            end)
        end)
    end
end

local c_config = {
    {
        name = 'Launch',
        type = 'lldb',
        request = 'launch',
        cwd = '${workspaceFolder}',
        stopOnEntry = false,
        runInTerminal = false,
        program = async(function (done_cb)
            ---@format disable-next
            vim.ui.input({
                prompt = 'Path to executable',
                default = vim.fn.getcwd() .. '/',
            }, function (path)
                done_cb(path)
            end)
        end),
        args = async(function (done_cb)
            ---@format disable-next
            vim.ui.input({ prompt = 'Program args' }, function (argstr)
                local args = {} --[=[@as string[]]=]
                if argstr and argstr:gsub('%s+', ''):len() > 0 then
                    args = vim.fn.split(argstr, ' ', true) --[=[@as string[]]=]
                end
                done_cb(args)
            end)
        end),
    },
}

dap.configurations.c = c_config
dap.configurations.cpp = c_config
dap.configurations.rust = c_config
