local dap = require('dap')

---@param fn fun(done_cb: fun(...: any))
---@return fun():thread
local async = function (fn)
    return function ()
        return coroutine.create(function (dap_run_co)
            fn(function (...)
                coroutine.resume(dap_run_co, ...)
            end)
        end)
    end
end

--resolve_lsp {{{
---@param client vim.lsp.Client
---@param done_cb fun(...:any)
---@return fun(err: lsp.ResponseError?, result: string?)
local function resolve_lsp(client, done_cb)
    return function (err, result)
        local ver
        if result then
            ver = result
        else
            if err then
                local msg = {
                    'Failed request workspace/executeCommand lua.getConfig',
                    '',
                }

                local e = tostring(err):gsub('\\(\n)\\9', '%1')
                for str in vim.gsplit(e, '\n') do
                    msg[#msg+1] = str
                end

                vim.notify(
                    msg,
                    'ERROR',
                    { title = 'local-lua-dbg: lsp_luaver' }
                )
            end
            vim.notify(
                'Using fallback configuration',
                'WARN',
                { title = 'local-lua-dbg: lsp_luaver' }
            )

            ver = client.config.settings.Lua.runtime.version --[[@as string]]
        end
        done_cb { lua = ver:gsub('%s+', ''):lower(), file = '${file}' }
    end
end
--}}}

--resolve_input {{{
---@param done_cb fun(...:any)
---@return fun(version: string)
local function resolve_input(done_cb)
    return function (version)
        if not version or version == '' then
            vim.notify('No input', 'ERROR', { title = 'DAP' })
            done_cb(nil)
            return
        end

        ---@type string
        local ver
        if version:match('[jJ][iI][tT]') then
            ver = 'luajit'
        else
            local ok, v = pcall(vim.version.parse, version)
            if ok then
                ---@cast v -nil
                ver = string.format('lua%d.%d', v.major, v.minor)
            else
                vim.notify(
                    {
                        'Invalid lua version: ' .. version,
                        '',
                        'Falling back to "lua" executable',
                    }, 'WARN', { title = 'DAP' }
                )
                ver = 'lua'
            end
        end

        done_cb { lua = ver, file = '${file}' }
    end
end
--}}}

dap.configurations.lua = {
    {
        name = 'Current file (local-lua-dbg, lua)',
        type = 'local-lua',
        request = 'launch',
        cwd = '${workspaceFolder}',
        program = async(function (done_cb)
            local buf = 0 -- vim.api.nvim_get_current_buf() -> doesn't work?!
            local c = vim.lsp.get_clients {
                name = 'lua_ls',
                bufnr = buf,
            }[1]
            if c and c:supports_method('workspace/executeCommand') then
                c:request(
                    'workspace/executeCommand',
                    {
                        command = 'lua.getConfig',
                        arguments = {
                            {
                                uri = vim.uri_from_bufnr(buf),
                                key = 'Lua.runtime.version',
                            },
                        },
                    },
                    resolve_lsp(c, done_cb),
                    buf
                )
            else
                vim.ui.input({ prompt = 'Lua version' }, resolve_input(done_cb))
            end
        end),
        args = {},
    },
}
