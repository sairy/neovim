local dap = require('dap')

-- Adapters
dap.adapters.lldb = {
    type = 'executable',
    command = 'lldb-vscode',
    name = 'lldb',
}

dap.adapters['local-lua'] = {
    type = 'executable',
    command = 'local-lua-dbg',
    enrich_config = function (config, on_config)
        if not config.extensionPath then
            -- 💀 If this is missing or wrong you'll see "module 'lldebugger' not found"
            -- errors in the dap-repl when trying to launch a debug session
            ---@diagnostic disable-next-line: inject-field
            config.extensionPath = '/usr/lib/node_modules/local-lua-debugger-vscode/'
        end
        on_config(config)
    end,
}

local configs = { 'c', 'lua' }

for _, lang in ipairs(configs) do
    require('plugconfig.dap.configs.' .. lang)
end
