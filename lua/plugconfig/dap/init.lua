local ok, dap = pcall(require, 'dap')

if not ok then
    return
end

local icons = require('kind')
local palette = require('catppuccin.palettes').get_palette() --[[@as CtpColors<string>]]

-- We need to override nvim-dap's default highlight groups,
-- AFTER requiring nvim-dap for catppuccin.
vim.api.nvim_set_hl(0, 'DapStopped', { fg = palette.green })

local signs = {
    { name = 'DapBreakpoint', text = icons.dap.breakpoint },
    { name = 'DapStopped',    text = icons.dap.stopped },
    { name = 'DapLogPoint',   text = icons.dap.log_point },
    {
        name = 'DapBreakpointCondition',
        hl = 'DapBreakpoint',
        text = icons.dap.breakpoint_condition,
    },
    {
        name = 'DapBreakpointRejected',
        hl = 'DapBreakpoint',
        text = icons.dap.breakpoint_rejected,
    },
}

for _, sign in ipairs(signs) do
    vim.fn.sign_define(sign.name, {
        text = sign.text,
        texthl = sign.hl or sign.name,
        linehl = '',
        numhl = '',
    })
end

require('plugconfig.dap.configs')

-- Mappings
local picker = require('util.picker')
local util = require('util.common')

---@format disable-next
local extra = picker {
    prompt = 'DAP extra options',
    items = {
        {
            name = '󰉥 Clear breakpoints',
            action = dap.clear_breakpoints,
        },
        {
            name = ' List breakpoints',
            action = function ()
                return dap.list_breakpoints(true)
            end,
        },
        {
            name = '󰆿 Run to cursor',
            action = dap.run_to_cursor,
        }
    },
    config = {
        telescope = {
            theme = 'dropdown',
        }
    }
}

util.bind('n', '<leader>de', extra, { noremap = true, silent = true })

util.bind('n', '<F9>', dap.step_into)
util.bind('n', '<F10>', dap.step_out)
util.bind('n', '<F11>', dap.step_over)

util.bind('n', '<leader>dr', dap.continue)
util.bind('n', '<leader>dt', dap.terminate)

util.bind('n', '<leader>db', dap.toggle_breakpoint)
util.bind('n', '<leader>dB', function ()
    vim.ui.input({ prompt = 'Breakpoint condition' }, function (cond)
        return dap.set_breakpoint(cond)
    end)
end)
