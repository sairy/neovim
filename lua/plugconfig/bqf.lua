--  _            __  --
-- | |__   __ _ / _| --
-- | '_ \ / _` | |_  --
-- | |_) | (_| |  _| --
-- |_.__/ \__, |_|   --
--           |_|     --

local ok, bqf = pcall(require, 'bqf')

if not ok then
    return
end

bqf.setup {
    auto_enable = true,
    auto_resize_height = true,
    magic_window = true,

    preview = {
        auto_preview = true,

        border_chars = { '│', '│', '─', '─', '╭', '╮', '╰', '╯', '█' },
        show_title = true,

        delay_syntax = 50,

        border = 'rounded',
        win_height = 12,
        win_vheight = 12,
        winblend = 0,
        wrap = true,
        buf_label = true,
        show_scroll_bar = true,

        should_preview_cb = function (bufnr)
            local bname = vim.api.nvim_buf_get_name(bufnr) --[[@as string]]
            local fsize = vim.fn.getfsize(bname)

            return fsize < bit.lshift(100, 10) and not bname:match('^fugitive://')
        end,
    },

    func_map = {
        open = 'e',
        split = '<C-x>',
        vsplit = '<C-v>',

        prevfile = '<C-p>',
        nextfile = '<C-n>',

        ptogglemode = 'zp',
        ptoggleitem = 'p',
        ptoggleauto = 'P',
    },
}
