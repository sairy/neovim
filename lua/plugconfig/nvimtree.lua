--             _           _                  --
--  _ ____   _(_)_ __ ___ | |_ _ __ ___  ___  --
-- | '_ \ \ / / | '_ ` _ \| __| '__/ _ \/ _ \ --
-- | | | \ V /| | | | | | | |_| | |  __/  __/ --
-- |_| |_|\_/ |_|_| |_| |_|\__|_|  \___|\___| --
--                                            --
local ok, nvimtree = pcall(require, 'nvim-tree')

if not ok then
    return
end

---@class NvimTree.Node
---@field absolute_path string
---@field executable    boolean
---@field extension     string
---@field link_to       string
---@field name          string
---@field type          'directory'|'file'|'link'

---@param lnode NvimTree.Node
---@param rnode NvimTree.Node
---@return boolean # lnode < rnode
local natsort = function (lnode, rnode)
    -- Directories always first
    if lnode.type ~= rnode.type
        and (lnode.type == 'directory' or rnode.type == 'directory')
    then
        return lnode.type == 'directory'
    end

    local left, right = lnode.name, rnode.name

    if left == right then
        return false
    end

    for i = 1, math.max(#left, #right) do
        local l, r = left:sub(i, -1), right:sub(i, -1)

        if tonumber(l:sub(1, 1)) and tonumber(r:sub(1, 1)) then
            local lnum = tonumber(l:match('^[0-9]+'))
            local rnum = tonumber(r:match('^[0-9]+'))

            if lnum ~= rnum then
                return lnum < rnum
            end
        elseif l:sub(1, 1) ~= r:sub(1, 1) then
            return l < r
        end
    end

    return false
end

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

nvimtree.setup {
    auto_reload_on_write = true,
    reload_on_bufenter = true,
    respect_buf_cwd = true,

    disable_netrw = true,
    hijack_netrw = true,
    hijack_cursor = true,
    hijack_unnamed_buffer_when_opening = false,

    hijack_directories = {
        enable = true,
        auto_open = true,
    },

    open_on_tab = false,

    -- sort_by = 'case_sensitive',
    sort_by = function (nodes)
        table.sort(nodes, natsort)
    end,

    update_cwd = true,

    update_focused_file = {
        enable = true,
        update_cwd = true,
        ignore_list = { 'term', 'Fterm' },
    },

    system_open = {
        cmd = 'setsid',
        args = { '-f', 'xdg-open' },
    },

    diagnostics = {
        enable = true,
        show_on_dirs = true,
        debounce_delay = 50,
        icons = {
            error = '',
            warning = '󰀪',
            info = '󰌶',
            hint = '󰋽',
        },
    },

    git = {
        enable = true,
        ignore = true,
        timeout = 500,
    },

    filters = {
        dotfiles = true,
        custom = {},
        exclude = {},
    },

    actions = {
        use_system_clipboard = true,
        change_dir = {
            enable = true,
            global = false,
            restrict_above_cwd = false,
        },
        expand_all = {
            max_folder_discovery = 300,
            exclude = {},
        },
        file_popup = {
            open_win_config = {
                relative = 'cursor',
                border = 'rounded',
                col = 1,
                row = 1,
            },
        },
        open_file = {
            quit_on_open = true,
            resize_window = true,
            window_picker = {
                enable = false,
                chars = '',
                exclude = {
                    filetype = {
                        'notify',
                        'packer',
                        'qf',
                        'diff',
                        'fugitive',
                        'fugitiveblame',
                    },
                    buftype = {
                        'nofile',
                        'terminal',
                        'help',
                    },
                },
            },
        },
        remove_file = {
            close_window = true,
        },
    },

    view = {
        width = 30,
        adaptive_size = false,
        centralize_selection = false,
        side = 'left',
        preserve_window_proportions = true,
        number = false,
        relativenumber = false,
        signcolumn = 'yes',
        float = {
            enable = false,
            open_win_config = {
                relative = 'editor',
                border = 'rounded',
                width = 30,
                height = 30,
                row = 1,
                col = 1,
            },
        },
    },

    renderer = {
        add_trailing = false,
        group_empty = false,
        full_name = false,
        highlight_git = true,
        highlight_opened_files = 'name',
        symlink_destination = true,
        -- root_folder_label = true,
        root_folder_modifier = ':~',
        indent_width = 2,
        indent_markers = {
            enable = true,
            inline_arrows = true,
            icons = {
                corner = '└',
                edge = '│',
                none = ' ',
                item = '│',
                bottom = '─',
            },
        },
        icons = {
            webdev_colors = true,
            git_placement = 'before',
            padding = ' ',
            symlink_arrow = ' 󰁔 ',
            show = {
                file = true,
                folder = true,
                folder_arrow = true,
                git = true,
            },
            glyphs = {
                default = '',
                symlink = '',
                bookmark = '󰆤',
                folder = {
                    arrow_closed = '',
                    arrow_open = '',
                    default = '',
                    open = '',
                    empty = '',
                    empty_open = '',
                    symlink = '',
                    symlink_open = '',
                },
                git = {
                    unstaged = '✗',
                    staged = '✓',
                    unmerged = '',
                    renamed = '➜',
                    untracked = '',
                    deleted = '',
                    ignored = '◌',
                },
            },
        },
        special_files = {
            'GNUmakefile',
            'Makefile',
            'makefile',
            'README',
            'README.md',
            'readme.md',
            'LICENSE',
            'COPYING',
        },
    },

    on_attach = function (bufnr)
        local bufbind = require('util.common').bufbind
        local api = require('nvim-tree.api')

        ---@param shortcut string
        ---@param action string|fun()
        ---@param desc string?
        local map = function (shortcut, action, desc)
            local opts = {
                desc = desc and 'nvim-tree: ' .. desc,
                noremap = true,
                silent = true,
                nowait = true,
            }
            return bufbind(bufnr, 'n', shortcut, action, opts)
        end

        --- Buffer local keys
        local keys = {
            { 'q',     api.tree.close,                   'Close' },
            { 'l',     api.node.open.edit,               'Open' },
            { ' ',     api.node.open.edit,               'Open' },
            { 'e',     api.node.open.edit,               'Open' },
            { 'f',     api.live_filter.start,            'Filter' },
            { 'F',     api.live_filter.clear,            'Clean Filter' },
            { 'h',     api.node.navigate.parent_close,   'Close Directory' },
            { 'H',     api.tree.collapse_all,            'Collapse' },
            { 'P',     api.node.navigate.parent,         'Parent Directory' },
            { 'J',     api.tree.change_root_to_node,     'CD' },
            { 'K',     api.tree.change_root_to_parent,   'Up' },
            { '<Tab>', api.node.open.preview,            'Open Preview' },
            { '<C-x>', api.node.open.horizontal,         'Open: Vertical Split' },
            { 'v',     api.node.open.vertical,           'Open: Vertical Split' },
            { 't',     api.node.open.tab,                'Open: New Tab' },
            { 'R',     api.tree.reload,                  'Refresh' },
            { '.',     api.node.run.cmd,                 'Run Command' },
            { 'g?',    api.node.show_info_popup,         'Info' },
            { 'm',     api.marks.toggle,                 'Toggle Bookmark' },
            { 'M',     api.marks.bulk.move,              'Move Bookmarked' },
            { 'y',     api.fs.copy.node,                 'Copy' },
            { 'x',     api.fs.cut,                       'Cut' },
            { 'p',     api.fs.paste,                     'Paste' },
            { 'a',     api.fs.create,                    'Create' },
            { 'r',     api.fs.rename,                    'Rename' },
            { 'd',     api.fs.remove,                    'Delete' },
            { 'Y',     api.fs.copy.filename,             'Copy Name' },
            { 'gy',    api.fs.copy.absolute_path,        'Copy Absolute Path' },
            { 'o',     api.node.run.system,              'Run System' },
            { 'zh',    api.tree.toggle_hidden_filter,    'Toggle Dotfiles' },
            { 'zi',    api.tree.toggle_gitignore_filter, 'Toggle Git Ignore' },
            { 'z?',    api.tree.toggle_help,             'Help' },
        }

        for _, key in ipairs(keys) do
            map(key[1], key[2], key[3])
        end
    end,
}

local utils = require('util.common')

-- Replace vim.notify usage with nvim_echo()
if utils.hasplugin('ui.notify') then
    local notify = require('nvim-tree.notify')

    for _, mode in ipairs { 'trace', 'debug', 'info', 'warn', 'error' } do
        if notify[mode] ~= nil then
            notify[mode] = vim.schedule_wrap(function (msg)
                local hl = ('Notify%sBorder'):format(mode:upper())
                return vim.api.nvim_echo({ { msg, hl } }, false, {})
            end)
        end
    end
end

--- Global Keys
utils.bind('n', '<leader>e', function ()
    return require('nvim-tree.api').tree.toggle()
end)
