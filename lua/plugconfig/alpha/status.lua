local M = {}

---Count the number of installed plugins
---@return { loaded: integer, total: integer }
local plugcount = function ()
    local stats = require('lazy').stats()

    return {
        loaded = stats.loaded,
        total = stats.count,
    }
end

---Returns the current nvim version
---@return string #vmajor.minor.patch (release|nightly)
local vim_ver = function ()
    local ver = vim.version()

    return string.format(
        'v%s (%s)',
        table.concat({ ver.major, ver.minor, ver.patch }, '.'),
        ver.prerelease and 'nightly' or 'release'
    )
end

---@return number #startuptime in ms
local startuptime = function ()
    return require('lazy').stats().startuptime
end

---Returns the current user and host names
---@return string #user
---@return string #host
local user_host_two = function ()
    return
        (os.getenv('USER') or 'anon'),
        (vim.uv.os_gethostname() or 'void')
end

---Returns the current user and host names
---@return string # user@host
local user_host = function ()
    return table.concat({ user_host_two() }, '@')
end

M = {
    plugcount = plugcount,
    vim_ver = vim_ver,
    startuptime = startuptime,

    user_host = user_host,
    user_host_two = user_host_two,
}

return M
