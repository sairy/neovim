--      _           _     _                         _  --
--   __| | __ _ ___| |__ | |__   ___   __ _ _ __ __| | --
--  / _` |/ _` / __| '_ \| '_ \ / _ \ / _` | '__/ _` | --
-- | (_| | (_| \__ \ | | | |_) | (_) | (_| | | | (_| | --
--  \__,_|\__,_|___/_| |_|_.__/ \___/ \__,_|_|  \__,_| --
--                                                     --
local dashboard = require('alpha.themes.dashboard')

local bookmarks = require('plugconfig.alpha.bookmarks')
local status = require('plugconfig.alpha.status')

local function _redraw()
    if vim.bo.filetype == 'alpha' then
        require('alpha').redraw()
    end
end

-- dashboard.button() {{{
---Better implementation of dashboard.button
---@param sc string
---@param txt string
---@param keybind fun()|string|string[]
---@param keybind_opts table<string,boolean>?
---@return table
local function button(sc, txt, keybind, keybind_opts)
    local sc_ = sc:gsub('%s', ''):gsub('SPC', '<leader>')

    local opts = {
        position = 'center',
        shortcut = sc,
        cursor = 3, --txt:find('[a-zA-Z]')-1 or 1,
        width = 50,
        align_shortcut = 'right',
        hl = 'AlphaButtons',
        hl_shortcut = 'AlphaShortcut',
    }

    keybind_opts = vim.tbl_extend('keep', keybind_opts or {}, {
        noremap = true,
        silent = true,
        nowait = true,
    })

    ---@type fun()
    local on_press

    if type(keybind) == 'function' then
        on_press = keybind
        keybind = ''
        keybind_opts.callback = on_press
    else
        if type(keybind) == 'table' then
            keybind = table.concat(keybind, ' ')
        end
        if not keybind:match('^:.*<[cC][rR]>$') then
            keybind = string.format(':%s<CR>', keybind)
        end

        on_press = function ()
            local key = vim.api.nvim_replace_termcodes(
                keybind or sc_ .. '<Ignore>',
                true,
                false,
                true
            )
            vim.api.nvim_feedkeys(key, 't', false)
        end
    end

    opts.keymap = { 'n', sc_, keybind, keybind_opts }

    return {
        type = 'button',
        val = txt,
        on_press = on_press,
        opts = opts,
    }
end
-- }}}

--- Config ---

-- Header {{{
local header = {
    type = 'text',
    val = {
        -- Rose {{{
        --[==[
        [[       :                       ..,,..    ...,,..]],
        [[      ,%,                .. ,#########::#########:,]],
        [[      :#%%,           ,,:',####%%%%%%##:`::%%%%####,]],
        [[     ,##%%%%,      ,##%% ,##%%%:::::''%' `::::%%####,]],
        [[     %###%%;;,   ,###%%:,##%%:::''    '  . .`:::%%###,]],
        [[    :####%%;;:: ,##%:' ,#%::''   .,,,..    . .`::%%%##,]],
        [[    %####%;;::,##%:' ,##%''  ,%%########%     . `:::%%##,]],
        [[    ######:::,##%:',####:  ,##%%:''     `%%,     .`::%%##,]],
        [[    :#####%:'##%:',#####' ,###%' ,%%%%,%%,'%,     . ::%%###,,..]],
        [[     #####%:,#%:'#######  %%:'%  %'  `%% %% %%,.     '::%%#######,]],
        [[     `####%,#%:',####### ::' %   ' ,%%%%%%, ::%%.    . '::%%######]],
        [[      `###'##%: ######## ,.   %%  %%,   ':: `:%%%  :  . .:::%%###']],
        [[      ,,::,###  %%%%%### ::  % %% '%%%,.::: .:%%%   #.  . ::%%%#']],
        [[,,,:::%%##:;#   `%%%%%## :% ,%, %   ':%%:'  #%%%' ,.:##.  ::%#']],
        [[::%%#####% %%:::  :::%%% `%%,'%%     ..,,%####' :%# `::##, '']],
        [[###%%::'###%::: .   `:::, `::,,%%%######%%'',::%##' ,:::%##]],
        [[''''   ,####%:::. .  `::%,     '':%%::' .,::%%%#'   :::%%%##,]],
        [[      :#%%'##%:::.  . . "%::,,.. ..,,,,::%%%###'  ,:%%%%####']],
        [[     ,###%%'###%:::: . . `::::::::::%%%#####'   ,::%####:']],
        [[     %###%%;'###%::::.   .`::%%%%%%%#####:'  ,,::%%##:']],
        [[     ####%;:;'####%:::::.   `:%######::'  ,,:::%%###]],
        [[     %####;:;'######%%::::.           ,::::%%%####']],
        [[     `####%;:'`#########%%:::....,,:::%%%#######']],
        [[        ;#####;;'..;;:::#########::%%#########:"']],
        [[                       ~~~~``````''''~~~]],
        --]==]
        -- }}}

        -- Moon {{{
        --[==[
        [[                                                                             ]],
        [[                               aaaaaaaaaaaaaaaa                               ]],
        [[                           aaaaaaaaaaaaaaaaaaaaaaaa                            ]],
        [[                        aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa                         ]],
        [[                      aaaaaaaaaaaaaaaaa           aaaaaa                       ]],
        [[                    aaaaaaaaaaaaaaaa                  aaaa                     ]],
        [[                   aaaaaaaaaaaaa aa                      aa                    ]],
        [[                 aaaaaaaa      aa                        a                   ]],
        [[                  aaaaaaa aa aaaa                                              ]],
        [[                aaaaaaaaa     aaa                                             ]],
        [[                 aaaaaaaaaaa aaaaaaa                                          ]],
        [[                 aaaaaaa    aaaaaaaaaa                                        ]],
        [[                 aaaaaa a aaaaaa aaaaaa                                        ]],
        [[                  aaaaaaa  aaaaaaa                                             ]],
        [[                  aaaaaaaa                                a                   ]],
        [[                   aaaaaaaaaa                            aa                    ]],
        [[                    aaaaaaaaaaaaaaaa                  aaaa                     ]],
        [[                      aaaaaaaaaaaaaaaaa           aaaaaa                      ]],
        [[                       aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa                         ]],
        [[                           aaaaaaaaaaaaaaaaaaaaaaaa                            ]],
        [[                              aaaaaaaaaaaaaaaa                                ]],
        --]==]
        -- }}}

        -- Quinn {{{
        ---@see https://katiefrogs.github.io/Dots-Converter/retro.html
        ---[==[
        [[⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⠤⠔⢒⡚⠉⠉⠉⠉⠉⠉⠒⠢⠼⡇⡞⠀⢀⣿⠀⣠⠞⠁⣠     ]],
        [[⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠴⢊⡡⠔⠂⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠑⢦⣾⣿⢰⠃⢀⣼⣿⠀⠀⡴  ]],
        [[⠀⠀⠀⠀⠀⠀⠀⣠⣪⠕⠊⠁⢀⣀⣤⣴⣶⡿⠟⠋⠁⠀⠀⠀⢀⠀⠀⠀⠀⠀⠨⡻⡇⣠⣿⣿⡇     ]],
        [[⠀⢸⣦⣤⣤⣐⠊⠉⠀⣀⣴⣾⣿⣿⣿⠟⠉⠀⠀⠀⢀⠂⠀⢠⢸⡀⠀⢂⠀⠀⠀⠈⢼⣿⣿⣿⠃     ]],
        [[⠀⠀⠙⠿⠿⢿⣿⣶⣾⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀⢀⠃⠀⠀⢸⢸⣥⠀⠀⢂⠀⠀⠀⠀⢻⣿⣿      ]],
        [[⠀⠀⠀⠀⣰⣿⣿⣿⣿⣿⣿⡿⠃⠀⠀⠀⠀⠀⠀⠂⠀⠀⠀⡘⣸⣿⣧⠀⠈⠆⠀⠀⠀⠀⢻⣯      ]],
        [[⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣅⠀⠀⠀⠀⠀⠀⠄⠀⠀⠀⢠⣇⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⠈⡌⣦⠤⠤⠤  ]],
        [[⠀⠀⣸⣿⣿⣿⣿⣿⣿⣿⣿⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣿⣽⢿⠟⠉⠀⠱⣀⢡⠀⠀⠀⠀⠇⢸⣇    ]],
        [[⠀⢠⣿⣿⣿⣿⣿⣿⣿⣿⡿⠀⠀⠀⠀⠀⢰⠀⠀⢀⣴⠿⡫⠃⠁⢀⣤⣾⣿⣿⣷⣷⣤⠀⠀⡀⢱⠻⡀   ]],
        [[⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⠇⠀⠀⠀⠀⢠⣾⣠⠴⠋⢁⠬⠊⢀⣴⣿⠟⡛⠉⣿⣿⣿⣿⣧⠀⡇⢈⢆⢧   ]],
        [[⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⢀⣴⣿⣿⡇⠀⠀⠈⠀⢰⣿⠿⠁⠀⡇⠀⣿⡟⣿⡟⣿⣦⡥⠚⡜⢸   ]],
        [[⠀⢹⣿⢿⣿⣿⣿⣿⣿⡇⢀⣠⣶⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⠺⣿⣷⠟⠀⢿⡏⠀⣠⠃⣸   ]],
        [[⠀⠈⠋⠈⣿⣿⣿⠿⢿⣷⣿⣿⣿⣿⣿⣿⡟⠐⠀⠀⠀⠀⠀⠀⠀⠀⠠⠤⠿⠴⠋⠀⢰⣿⣷⠊⠈⣷⠁   ]],
        [[⠀⠀⠀⠀⠘⢿⣿⢆⠘⢿⣿⣿⣿⣿⠟⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢈⣿⣿⣧⣶⠁    ]],
        [[⣀⣤⣶⣾⣵⣄⠁⠀⣹⣿⡀⠀⠀⠹⣆⠀⠀⠐⠒⠂⠈⠁⠀⠀⠀⠀⠀⠀⠀⠀⢀⡴⢫⣏⡛⣩⠿⡄    ]],
        [[⣿⣿⣿⣿⣿⣿⣆⠀⠙⢿⣿⣶⣄⡀⠈⠂⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⡒⠁⠀⣎⡴⠛⠳⢤⡇⢀⡀  ]],
        [[⣿⢿⣿⣿⣿⣿⣿⣆⡀⣼⣛⡿⠛⣌⠑⢢⣤⣀⠀⠀⠀⠀⢀⣀⣤⠖⠊⢁⢿⡝⠦⣀⠹⣄⠀⠀⣠⣷⣾⡇  ]],
        [[⣿⣿⣿⣿⣭⣾⡎⢝⣫⣧⠞⠉⠉⡟⠀⠀⠹⣆⠉⠻⣭⣿⣿⡿⢋⣤⣾⣿⢗⣵⣿⣎⣶⣿⣷⣾⣿⣿⣿⠃  ]],
        [[⣿⢿⣿⣿⣷⡟⣓⠞⣤⣭⣷⣤⡮⣀⡞⡖⣤⣹⣧⣼⣿⣿⣧⣾⣿⣿⣟⣵⣿⣿⡿⣽⣿⣿⣿⣿⣿⣿⠁   ]],
        [[⣟⡺⣥⣿⡿⣿⢿⣧⣿⡆⣿⣿⣟⢯⣷⣽⣿⣿⣿⣿⣿⣿⣝⠿⣿⣿⣾⣿⠿⣫⣽⣿⣿⣿⣿⣿⠏⠀⢳⡀  ]],
        --]==]
        --}}}
    },

    opts = {
        position = 'center',
        hl = 'AlphaHeader',
    },
}
-- }}}

-- Buttons {{{
local buttons = {
    type = 'group',
    val = {
        {
            type = 'text',
            val = 'Quick Actions',
            opts = {
                position = 'center',
                hl = 'AlphaHeaderLabel',
            },
        },

        { type = 'padding', val = 1 },

        {
            type = 'group',
            val = {
                button('e', '  New file', 'ene'),

                button('b', '  Bookmarks', bookmarks.picker),

                button('f', '󰈙  Find files', {
                    'Telescope',
                    'find_files',
                    'initial_mode=insert',
                    -- 'theme=dropdown'
                }),
                button('g', '  Recent files', {
                    'Telescope',
                    'oldfiles',
                    'only_cwd=true',
                    'prompt_title=Recent\\ Files',
                    -- 'theme=dropdown'
                }),

                button('u', '  Update plugins', 'Lazy update'),
                button('h', '  Check health', 'checkhealth'),
                button('q', '󰿅  Quit NeoVim', 'qa'),
            },

            opts = {
                spacing = 1,
            },
        },
    },
}
-- }}}

-- Footer {{{
local footer = {
    type = 'group',
    val = {
        {
            type = 'text',
            val = function ()
                return string.format(' %s', status.user_host():lower())
            end,

            opts = {
                position = 'center',
                hl = 'AlphaFooter',
            },
        },
        {
            type = 'text',
            ---@return string
            val = function ()
                local version = status.vim_ver()
                local startup = status.startuptime()
                local plugs = status.plugcount()

                return string.format(
                    '%s %s  %s %.2fms  %s %d/%d plugins',
                    '', version,
                    '󰁫', startup,
                    '', plugs.loaded, plugs.total
                )
            end,

            opts = {
                position = 'center',
                hl = 'AlphaFooter',
            },
        },
    },

    opts = {
        spacing = 1,
    },
}
-- }}}

--- Initialize config ---

dashboard.config = {
    layout = {
        { type = 'padding', val = 2 },
        header,
        { type = 'padding', val = 2 },
        buttons,
        { type = 'padding', val = 1 },
        footer,
    },

    opts = {
        margin = 3,
        redraw_on_resize = true,
        setup = function ()
            local buf = vim.api.nvim_get_current_buf()
            local utils = require('util.common')
            utils.aucmd({ 'VimEnter', 'DirChanged' }, {
                buffer = buf,
                callback = _redraw,
                group = utils.getaugroup('alpha'),
            })

            utils.aucmd('User', {
                pattern = 'LazyVimStarted',
                once = true,
                callback = _redraw,
                desc = 'Update lazy stats when done computing',
            })
        end,
    },
}

return dashboard.config
