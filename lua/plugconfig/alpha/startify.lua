--      _             _   _  __        --
--  ___| |_ __ _ _ __| |_(_)/ _|_   _  --
-- / __| __/ _` | '__| __| | |_| | | | --
-- \__ \ || (_| | |  | |_| |  _| |_| | --
-- |___/\__\__,_|_|   \__|_|_|  \__, | --
--                              |___/  --
local startify = require('alpha.themes.startify')
-- local list, fort_lst = pcall(require, 'plugconfig.alpha.fortunes')

-- Header {{{

local header = {
    type = 'text',
    val = {
        --[===[
        [[           _            ]],
        [[  ___ ___ (_)_ __   _ _ ]],
        [[/  _   _  \ |  __)/ _  )]],
        [[| ( ) ( ) | | |  ( (_| |]],
        [[(_) (_) (_)_)_)   \__ _)]],
        [[                        ]],
        --]===]
        [[           _           ]],
        [[ _ __ ___ (_)_ __ __ _ ]],
        [[| '_ ` _ \| | '__/ _` |]],
        [[| | | | | | | | | (_| |]],
        [[|_| |_| |_|_|_|  \__,_|]],
        [[                       ]],
    },
    opts = {
        position = 'left',
        hl = 'Type',
        shrink_margin = false,
    },
}
-- }}}

-- Top Buttons {{{
local top_btns = {
    type = 'group',
    val = {
        startify.button('e', ' New file', ':ene<CR>'),
        startify.button('u', ' Update plugins', ':PackerSync<CR>'),
    },
}
-- }}}

-- Bookmarks {{{
local bookmarks = {
    type = 'group',
    val = {
        {
            type = 'text',
            val = 'Bookmarks',
            opts = { hl = 'SpecialComment', shrink_margin = false },
        },

        { type = 'padding', val = 1 },

        {
            type = 'group',
            val = {
                startify.button('n', '  NeoVim', ':bro e $NVIMCFG<CR>'),
                startify.button('z', '  zsh', ':bro e $ZDOTDIR<CR>'),
                startify.button('d', 'Ꟊ  dwm', ':bro e $dwm<CR>'),
                startify.button('s', '𝝲𝝺 StumpWM', ':bro e $swm<CR>'),
            },
        },
    },
}
-- }}}

-- Recent files {{{
local recents = {
    type = 'group',
    val = {
        {
            -- text above the files
            type = 'text',
            val = 'Recent Files',
            opts = { hl = 'SpecialComment' },
        },

        { type = 'padding', val = 1 },

        {
            -- files displayed
            type = 'group',
            val = function ()
                return {
                    startify.mru(0) }
            end,
        },
    },
}

-- }}}

-- Current directory {{{

local cwd = {
    ---[===[
    type = 'group',
    val = {
        {
            -- text displayed above the files
            type = 'text',
            val = function ()
                return 'Current directory (' .. vim.fn.getcwd() .. ')'
            end,

            opts = { hl = 'SpecialComment', shrink_margin = false },
        },

        { type = 'padding', val = 1 },

        {
            -- actual files
            type = 'group',
            val = function ()
                return { startify.mru(10, vim.fn.getcwd(), 6) }
            end,
            opts = { shrink_margin = false },
        },
    },
    --]===]
}
-- }}}

-- Bottom Buttons {{{
local bot_btns = {
    type = 'group',
    val = {
        startify.button('q', ' Quit Neovim', ':qa<CR>'),
    },
}
-- }}}

-- Footer {{{
-- local footer = {}
-- }}}

startify.config = {

    layout = {
        { type = 'padding', val = 1 },
        header,
        { type = 'padding', val = 1 },
        top_btns,
        { type = 'padding', val = 1 },
        bookmarks,
        { type = 'padding', val = 1 },
        recents,
        ---[===[
        { type = 'padding', val = 1 },
        cwd,
        --]===]
        { type = 'padding', val = 1 },
        bot_btns,
    },

    opts = {
        margin = 3,
        redraw_on_resize = true,
        setup = function ()
            local buf = vim.api.nvim_get_current_buf()
            local utils = require('util.common')
            utils.aucmd({ 'VimEnter', 'DirChanged' }, {
                buffer = buf,
                callback = function (args)
                    if vim.bo[args.buf].filetype == 'alpha' then
                        require('alpha').redraw()
                    end
                end,
                group = utils.getaugroup('alpha'),
            })
        end,
    },
}

return startify.config

-- EOF
