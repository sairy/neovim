--                            _            --
--   __  __        _ ____   _(_)_ __ ____  --
--  /  \/ / ____  | '_ \ \ / / | '_ ` _  \ --
-- ( ()  < |____| | | | \ V /| | | | | | | --
--  \__/\_\       |_| |_|\_/ |_|_| |_| |_| --
--                                         --
local ok, alpha = pcall(require, 'alpha')

if not ok then
    return
end

do
    local utils = require('util.common')

    local g = utils.augroup('alpha')

    utils.aucmd('FileType', {
        pattern = 'alpha',
        callback = function ()
            vim.wo.fillchars = 'eob:\\'
        end,
        group = g,
        desc = 'Remove "~" chars from lines',
    })

    utils.aucmd('User', {
        pattern = 'AlphaReady',
        callback = function (args)
            local stal = vim.o.showtabline
            vim.o.showtabline = 0
            utils.aucmd('BufUnload', {
                buffer = args.buf,
                once = true,
                callback = function ()
                    vim.o.showtabline = stal
                    return true
                end,
                group = g,
                desc = 'Re-display tabline after exiting alpha',
            })
        end,
        group = g,
        desc = 'Toggle tabline in alpha buffers',
    })
end


local cfg_ok, cfg = pcall(
    require,
    'plugconfig.alpha.' .. (vim.g.alpha_dashboard or '-')
)

if not cfg_ok then
    vim.notify(
        'unknown dashboard: ' .. vim.g.alpha_dashboard,
        vim.log.levels.ERROR,
        { title = 'α-nvim' }
    )
else
    alpha.setup(cfg)
end
