local picker = require('util.picker')

local M = {}

---@type picker.item[]
local default = {
    {
        name = '  NeoVim',
        action = 'bro e $NVIMCFG',
    },
    {
        name = '  dwm',
        action = 'bro e $dwm',
    },
    {
        name = '  zsh',
        action = 'bro e $ZDOTDIR',
    },
}

local bookmarks = default

local path = vim.fn.stdpath('config') .. '/bookmarks'
local sb = vim.uv.fs_stat(path)
if sb and sb.type == 'file' then
    local f = loadfile(path) --[[@as (fun():picker.item[])?]]
    bookmarks = f and f() or default
end

local bk_picker = picker {
    prompt = 'Choose a bookmark',
    items = bookmarks,
    config = {
        telescope = {
            theme = 'cursor',
            args = {
                layout_config = { width = 40 },
            },
        },
    },
}


M = {
    bookmarks = bookmarks,
    picker = bk_picker,
}

return M
