--  _       _                                 --
-- | |_ ___| | ___  ___  ___ ___  _ __   ___  --
-- | __/ _ \ |/ _ \/ __|/ __/ _ \| '_ \ / _ \ --
-- | ||  __/ |  __/\__ \ (_| (_) | |_) |  __/ --
--  \__\___|_|\___||___/\___\___/| .__/ \___| --
--                               |_|          --
local ok, telescope = pcall(require, 'telescope')

if not ok then
    return
end

local actions = require('telescope.actions')
local builtin = require('telescope.builtin')
local sorters = require('telescope.sorters')
local preview = require('telescope.previewers')

local defaults = {
    sorting_strategy = 'descending',
    selection_strategy = 'reset',
    scroll_strategy = 'cycle',

    layout_strategy = 'flex',
    layout_config = {
        flex = {
            width = 0.8,
            height = 0.9,
            prompt_position = 'bottom',

            flip_columns = 120,
        },
    },

    winblend = 0,
    border = true,
    wrap_results = false,

    prompt_prefix = '󰍉 ',
    selection_caret = '> ',
    entry_prefix = '  ',
    multi_icon = ' ',

    initial_mode = 'normal',

    path_display = {
        shorten = { len = 10, exclude = { -1, 1 } },
    },

    borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
    hl_result_eol = true,
    dynamic_preview_title = false,

    results_title = 'Results',
    prompt_title = 'Prompt',

    preview = {
        check_mime_type = true,
        filesize_limit = 30, -- MB
        timeout = 500,
        treesitter = true,
        msg_bg_fillchar = '/',
        hide_on_startup = false,
    },

    color_devicons = true,

    vimgrep_arguments = {
        'rg',
        '--color=never',
        '--no-heading',
        '--with-filename',
        '--line-number',
        '--column',
        '--engine=default',
    },

    file_sorter = sorters.get_fzy_sorter,
    file_ignore_patterns = { '.git' },
    generic_sorter = sorters.get_fzy_sorter,

    file_previewer = preview.vim_buffer_cat.new,
    grep_previewer = preview.vim_buffer_vimgrep.new,
    qflist = preview.vim_buffer_qflist.new,

    mappings = {
        n = {
            q = actions.close,
        },
    },
}

local pickers = {
    find_files = {
        follow = true,
        hidden = true,

        no_ignore = false,
        no_ignore_parent = false,
    },

    git_files = {
        use_git_root = true,
        show_untracked = true,
        recurse_submodules = false,
    },

    git_status = {
        use_git_root = true,
        git_icons = {
            added = '✓',
            changed = '✗',
            copied = '>',
            deleted = '',
            renamed = '➜',
            unmerged = '',
            untracked = '',
        },
    },

    help_tags = {
        lang = vim.o.helplang,
        fallback = true,
    },

    man_pages = {
        sections = { 'ALL' },
        cmd = { 'apropos', '.' },
    },

    -- most important of them all :)
    planets = {
        show_pluto = true,
        show_moon = true,
    },
}

local extensions = {}


telescope.setup {
    defaults = defaults,
    pickers = pickers,
    extensions = extensions,
}

--- Keys
local bind = require('util.common').bind

bind('n', '<leader>tb', function ()
    if not pcall(builtin.git_branches) then
        vim.notify('Current dir is not in a git tree!', vim.log.levels.INFO)
    end
end)

bind('n', '<leader>tf', builtin.find_files)
bind('n', '<leader>tm', builtin.man_pages)
bind('n', '<leader>th', builtin.help_tags)
bind('n', '<leader>tl', builtin.live_grep)

bind('n', 'z=', function ()
    require('telescope.builtin').spell_suggest(
        require('telescope.themes').get_cursor {
            layout_config = {
                width = 40,
            },
        }
    )
end)
