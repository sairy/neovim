--                           _             --
--  _ __ ___  ___  ___ _ __ (_)_ __   ___  --
-- | '__/ _ \/ __|/ _ \ '_ \| | '_ \ / _ \ --
-- | | | (_) \__ \  __/ |_) | | | | |  __/ --
-- |_|  \___/|___/\___| .__/|_|_| |_|\___| --
--                    |_|                  --

local ok, rose = pcall(require, 'rose-pine')

if not ok then
    return
end

rose.setup {
    dark_variant = 'moon',
    bold_vert_split = true,
    dim_nc_background = false,
    disable_background = true,
    disable_float_background = false,
    disable_italics = false,

    groups = {
        background = 'base',
        panel = 'surface',
        border = 'highlight_med',
        comment = 'muted',
        link = 'iris',
        punctuation = 'subtle',

        error = 'love',
        hint = 'iris',
        info = 'foam',
        warn = 'gold',

        headings = {
            h1 = 'iris',
            h2 = 'foam',
            h3 = 'rose',
            h4 = 'gold',
            h5 = 'pine',
            h6 = 'foam',
        }
        -- or set all headings at once
        -- headings = 'subtle'
    },

    -- Change specific vim highlight groups
    highlight_groups = {
        ColorColumn = { bg = 'rose' }
    }
}

vim.cmd('colorscheme rose-pine')
vim.cmd('highlight Folded gui=nocombine guifg=#c4a7e7 guibg=#45475a')


-- EOF
