--            _                              _        --
--   ___ __ _| |_ _ __  _ __  _   _  ___ ___(_)_ __   --
--  / __/ _` | __| '_ \| '_ \| | | |/ __/ __| | '_ \  --
-- | (_| (_| | |_| |_) | |_) | |_| | (_| (__| | | | | --
--  \___\__,_|\__| .__/| .__/ \__,_|\___\___|_|_| |_| --
--               |_|   |_|                            --
---@type boolean, Catppuccin
local ok, cat = pcall(require, 'catppuccin')

if not ok then
    return
end

local utils = require('util.common')

vim.g.catppuccin_flavour = 'mocha'

cat.setup {
    flavour = vim.g.catppuccin_flavour,

    background = {
        light = 'latte',
        dark = 'mocha',
    },

    compile_path = vim.fn.stdpath('cache') .. '/catppuccin',
    transparent_background = true,
    show_end_of_buffer = false,
    term_colors = true,
    kitty = vim.env.KITTY_WINDOW_ID and true or false,

    dim_inactive = {
        enabled = false,
        shade = 'dark',
        percentage = 0.25,
    },

    no_bold = false,
    no_italic = false,
    no_underline = false,

    styles = {
        comments = { 'italic' },
        conditionals = { 'bold' },
        loops = { 'bold' },
        functions = { 'italic' },
        keywords = { 'italic' },
        strings = {},
        variables = {},
        numbers = { 'bold' },
        booleans = { 'bold', 'italic' },
        properties = {},
        types = {},
        operators = { 'bold' },
    },

    default_integrations = false,

    ---@diagnostic disable-next-line: missing-fields
    integrations = {
        alpha = true,
        barbecue = {
            dim_dirname = false,
        },
        cmp = true,
        dap = true,
        dap_ui = true,
        dropbar = { enabled = false },
        fidget = true,
        gitsigns = true,
        headlines = true,
        indent_blankline = { enabled = false },
        markdown = true,
        native_lsp = {
            enabled = true,
            virtual_text = {
                errors = { 'italic' },
                hints = { 'italic' },
                warnings = { 'italic' },
                information = { 'italic' },
            },
            underlines = {
                errors = { 'undercurl' },
                hints = {},
                information = {},
                warnings = { 'undercurl' },
            },
            inlay_hints = {
                background = true,
            },
        },
        navic = {
            enabled = true,
            custom_bg = 'NONE',
        },
        notify = true,
        nvimtree = true,
        rainbow_delimiters = true,
        semantic_tokens = true,
        telescope = {
            enabled = true,
            style = 'classic',
        },
        treesitter = true,
    },
    color_overrides = {},
    highlight_overrides = {},

    custom_highlights = function (C)
        C.none = 'NONE'

        local U = require('catppuccin.utils.colors')
        local O = require('catppuccin').options

        local if_opaque = function (hl)
            return O.transparent_background and C.none or hl
        end

        --{{{ General Overrides
        ---@type { [string]: CtpHighlight }
        local overrides = {
            --- Semantic tokens
            PreProc = { fg = C.mauve },
            StorageClass = { fg = C.mauve },
            ['@modifier'] = { link = 'StorageClass' },
            ['@macro'] = { link = '@constant' },
            ['@annotation'] = {
                fg = C.peach,
                style = { 'bold' },
            },

            ['@importDeclaration'] = { style = { 'italic' } },

            ['@constructor'] = { link = '@type' },
            ['@constructor.typescript'] = { link = '@constructor' },

            ['@class'] = { link = '@type' },
            ['@enum'] = { link = '@type' },
            ['@enumMember'] = { link = '@constant' },
            ['@interface'] = {
                fg = C.yellow,
                style = { 'italic' },
            },

            ['@module'] = { link = '@namespace' },
            ['@module.builtin'] = { link = '@module' },
            ['@namespace'] = {
                fg = C.mauve,
                style = { 'bold' },
            },

            ['@field'] = { fg = C.teal },
            ['@property'] = { link = '@field' },
            ['@property.cpp'] = { link = '@property' },
            ['@method'] = { fg = C.blue },

            -- Workaround for treesitter#d0b9afe9f6ee1c3f2ed80cad1805be8b521d0b6a
            -- ['@type.qualifier'] = { fg = C.mauve },
            ['@keyword.modifier'] = { fg = C.mauve },

            -- Sumneko_lua diagnostics
            ['@event'] = {
                fg = C.green,
                style = { 'bold' },
            },

            ['@lsp.type.variable'] = {},
            ['@lsp.type.operator.lua'] = { link = '@operator' },
            ['@lsp.type.string'] = { link = '@string' },

            ['@lsp.mod.readonly.go'] = { link = '@constant' },

            ['@lsp.typemod.function.defaultLibrary'] = {},
            ['@lsp.typemod.function.defaultLibrary.lua'] = { link = '@function.builtin' },
            ['@lsp.typemod.method.defaultLibrary'] = {},

            -- TODO: find a good highlight for this?
            ['@lsp.typemod.variable.defaultLibrary'] = {},

            ---####---

            Folded = { fg = C.blue, bg = C.surface1 },

            NormalFloat = { bg = if_opaque(C.mantle) },
            Comment = { fg = C.overlay1 },
            LineNr = { fg = C.overlay1 },
            Pmenu = { bg = C.surface0, fg = C.overlay2 },


            CursorLine = {
                bg = O.transparent_background
                    and C.none
                    or U.vary_color(
                        { latte = U.lighten(C.mantle, 0.70, C.base) },
                        U.darken(C.surface0, 0.64, C.base)),
            },

            CursorLineNr = {
                fg = C.pink,
                style = { 'bold' },
            },

            -- Alpha
            AlphaShortcut = { fg = C.yellow },
            AlphaHeader = { fg = C.yellow },
            AlphaHeaderLabel = { fg = C.mauve },
            AlphaButtons = { fg = C.text },
            AlphaFooter = { fg = C.teal, style = {} },

            -- Bqf
            BqfPreviewFloat = { bg = C.mantle },

            -- LSP
            DiagnosticVirtualTextError = { bg = C.none },
            DiagnosticVirtualTextWarn = { bg = C.none },
            DiagnosticVirtualTextInfo = { bg = C.none },
            DiagnosticVirtualTextHint = { bg = C.none },

            -- Eyeliner.nvim
            EyelinerPrimary = {
                fg = C.pink,
                style = { 'bold' },
            },
            EyelinerSecondary = {
                fg = C.green,
                style = { 'underline' },
            },

            -- Fidget.nvim
            FidgetTask = { bg = C.none, fg = C.text },
            FidgetTitle = { fg = C.blue },

            -- Headlines.nvim
            Headline1 = { fg = C.red },
            Headline2 = { fg = C.peach },
            Headline3 = { fg = C.yellow },
            Headline4 = { fg = C.green },
            Headline5 = { fg = C.sapphire },
            Headline6 = { fg = C.mauve },

            -- Indent-blankline.nvim
            IndentLine = {
                fg = C.surface1,
                style = { 'nocombine' },
            },

            IndentLineCurrent = {
                fg = C.subtext0,
                style = { 'nocombine', 'bold' },
            },

            -- Notify
            NotifyBackground = { bg = C.base },
        }

        for i = 1, 6 do
            local name = 'Headline' .. i
            local hl = overrides[name]

            hl.bg = C.surface0
            if not hl.style then
                hl.style = {}
            end
            table.insert(hl.style, 'bold')

            overrides[name .. 'Reverse'] = { fg = hl.bg, bg = C.none }
        end
        ---}}}

        --{{{ Telescope Overrides
        ---@type { [string]: CtpHighlight }
        local telescope = O.transparent_background and {
            TelescopeBorder = {
                fg = C.blue,
                bg = C.none,
            },
            TelescopeNormal = { link = 'NormalFloat' },
        } or {
            TelescopePromptPrefix = {
                fg = C.blue,
                bg = C.surface0,
                style = { 'bold' },
            },

            TelescopePromptNormal = {
                fg = C.text,
                bg = C.surface0,
            },
            TelescopePromptBorder = {
                fg = C.surface0,
                bg = C.surface0,
            },

            TelescopePromptTitle = {
                bg = C.blue,
                fg = C.base,
                style = { 'bold' },
            },

            TelescopeSelection = {
                fg = C.teal,
                bg = C.surface0,
            },
            TelescopeSelectionCaret = {
                fg = C.teal,
                bg = C.surface0,
                style = { 'bold' },
            },

            TelescopePreviewNormal = { bg = C.mantle },
            TelescopePreviewBorder = {
                bg = C.mantle,
                fg = C.mantle,
            },
            TelescopePreviewTitle = {
                bg = C.mauve,
                fg = C.base,
                style = { 'bold' },
            },

            TelescopeResultsNormal = { bg = C.mantle },
            TelescopeResultsBorder = {
                bg = C.mantle,
                fg = C.mantle,
            },
            TelescopeResultsTitle = {
                bg = C.teal,
                fg = C.base,
                style = { 'bold' },
            },

            TelescopeResultsDiffAdd = { fg = C.green },
            TelescopeResultsDiffChange = { fg = C.yellow },
            TelescopeResultsDiffDelete = { fg = C.red },
        }
        ---}}}

        ---@see https://github.com/neovim/neovim/pull/22022
        local fixes = require('util.semtokens').compat {
            mod = { 'importDeclaration' },
            type = {
                'annotation',
                'class',
                'comment',
                'constructor',
                'enum',
                'enumMember',
                'event',
                'interface',
                'macro',
                'method',
                'modifier',
                'namespace',
            },
        }

        return vim.tbl_extend('force', overrides, fixes, telescope)
    end,
}
vim.cmd('colorscheme catppuccin')

utils.bind('n', '<leader>a', function ()
    ---@type CatppuccinOptions
    local O = require('catppuccin').options

    O.transparent_background = not O.transparent_background

    vim.cmd('silent CatppuccinCompile')
end)


local g = utils.augroup('catppuccin_theme')
utils.aucmd('BufWritePost', {
    pattern = 'catppuccin.lua',
    callback = function ()
        vim.cmd('silent CatppuccinCompile')
        vim.schedule(function ()
            vim.cmd('colorscheme catppuccin')
        end)
    end,
    group = g,
})


-- EOF
