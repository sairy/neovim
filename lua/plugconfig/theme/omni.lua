--                        _  --
--   ___  _ __ ___  _ __ (_) --
--  / _ \| '_ ` _ \| '_ \| | --
-- | (_) | | | | | | | | | | --
--  \___/|_| |_| |_|_| |_|_| --
--                           --

local ok, omni = pcall(require, 'omni')

if not ok then
    return
end

omni.setup()
vim.cmd('highlight Folded ctermbg=238 gui=nocombine')


-- EOF
