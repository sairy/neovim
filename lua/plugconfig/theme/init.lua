--  _   _                          --
-- | |_| |__   ___ _ __ ___   ___  --
-- | __| '_ \ / _ \ '_ ` _ \ / _ \ --
-- | |_| | | |  __/ | | | | |  __/ --
--  \__|_| |_|\___|_| |_| |_|\___| --
--                                 --

if not pcall(require, 'plugconfig.theme.' .. (vim.g.theme or '-')) then
    vim.notify(
        ('configuration for theme \'%s\' not found'):format(vim.g.theme),
        vim.log.levels.ERROR,
        { title = 'theme-setup' }
    )

    vim.cmd('colorscheme default')
end

-- EOF
