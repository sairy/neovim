--  _____ _____                    --
-- |  ___|_   _|__ _ __ _ __ ___   --
-- | |_    | |/ _ \ '__| '_ ` _ \  --
-- |  _|   | |  __/ |  | | | | | | --
-- |_|     |_|\___|_|  |_| |_| |_| --
--                                 --
---@diagnostic disable:assign-type-mismatch

local ok, fterm = pcall(require, 'FTerm')

if not ok then
    return
end

fterm.setup {
    ft = 'Fterm',

    cmd = vim.o.shell,

    border = 'rounded',

    auto_close = true,

    hl = 'Normal',

    blend = 0,

    dimensions = {
        width = 0.8,
        height = 0.8,
        x = 0.5,
        y = 0.5,
    },

    on_exit = nil,
    on_stdout = nil,
    on_stderr = nil,
    env = nil,
    clear_env = false,
}

local map = require('util.common').bind

map('n', '<C-Space>', fterm.toggle)
map('t', '<C-Space>', fterm.toggle)
