--  _                           _ _   _             --
-- | |_ _ __ ___  ___       ___(_) |_| |_ ___ _ __  --
-- | __| '__/ _ \/ _ \_____/ __| | __| __/ _ \ '__| --
-- | |_| | |  __/  __/_____\__ \ | |_| ||  __/ |    --
--  \__|_|  \___|\___|     |___/_|\__|\__\___|_|    --
--                                                  --
local ok, treesitter = pcall(require, 'nvim-treesitter.configs')
if not ok then
    return
end

--{{{ Parsers
local parsers = {
    'awk',
    'bash',
    'bibtex',
    'cmake',
    'comment',
    'commonlisp',
    'cpp',
    'css',
    'diff',
    'git_config',
    'git_rebase',
    'gitattributes',
    'gitcommit',
    'gitignore',
    'go',
    'haskell',
    'html',
    'ini',
    'java',
    'javascript',
    'json',
    --'latex',
    'luap',
    'make',
    'meson',
    'ninja',
    'passwd',
    'perl',
    'python',
    'regex',
    'rust',
    'svelte',
    'todotxt',
    'typescript',
    'xml',
    'yaml',
}
--}}}

treesitter.setup {
    -- One of "all", "maintained" (parsers with maintainers), or a list of languages
    ensure_installed = {},

    -- Install languages synchronously (only applied to `ensure_installed`)
    sync_install = false,

    auto_install = false,

    -- List of parsers to ignore installing
    ignore_install = {},

    highlight = {
        -- `false` will disable the whole extension
        enable = true,
        -- list of languages that will be disabled
        disable = {},
        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
    },

    modules = {},
    textobjects = {
        select = {
            enable = true,
            -- Automatically jump forward to textobj
            lookahead = true,
            keymaps = {
                ['ac'] = '@conditional.outer',
                ['ic'] = '@conditional.inner',
                ['al'] = '@loop.outer',
                ['il'] = '@loop.inner',
                ['af'] = '@function.outer',
                ['if'] = '@function.inner',
                ['aP'] = '@parameter.outer',
                ['iP'] = '@parameter.inner',
                ['aC'] = '@class.outer',
                ['iC'] = '@class.inner',
            },
            selection_modes = {
                ['@parameter.outer'] = 'v', -- charwise
                ['@function.outer'] = 'V',  -- linewise
                ['@class.outer'] = 'V',
            },
            include_surrounding_whitespace = true,
        },
        lsp_interop = {
            enable = true,
            border = 'rounded',
            peek_definition_code = {
                ['<leader>df'] = '@function.outer',
                ['<leader>dF'] = '@class.outer',
            },
        },
    },
}

vim.schedule(function ()
    vim.cmd.TSUpdate { args = parsers, mods = { silent = true } }
end)

vim.schedule(function ()
    local script = vim.fn.stdpath('config') .. '/ts-install-latex.sh'

    vim.uv.fs_stat(script, function (err)
        if err == nil then
            local cfg = require('nvim-treesitter.parsers').get_parser_configs()
            local url = cfg.latex.install_info.url

            local ts_dir = require('lazy.core.config').options.root .. '/ts-nvim'
            local rev_file = ts_dir .. '/parser-info/latex.revision'
            local dest = ts_dir .. '/parser'

            vim.system(
                { script },
                {
                    env = {
                        REPO = url,
                        REVFILE = rev_file,
                        BUILDDIR = vim.fn.stdpath('data') --[[@as string]],
                        INSTALLDIR = dest,
                    },
                },
                function (out)
                    if out.code ~= 0 then
                        vim.notify_schedule(tostring(out.stderr), 'ERROR', {
                            title = 'tree-sitter-latex update',
                        })
                    else
                        local stdout = vim.split(out.stdout, '\n')
                        local msg = stdout[#stdout - 1]

                        if msg:match('success') then
                            vim.notify_schedule(msg, 'INFO', {
                                title = 'tree-sitter-latex update',
                            })
                        end
                    end
                end
            )
        else
            vim.notify_schedule(tostring(err), 'ERROR', { title = 'uv.fs_stat' })
        end
    end)
end)
