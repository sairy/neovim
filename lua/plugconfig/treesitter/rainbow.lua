local ok, rainbow = pcall(require, 'rainbow-delimiters')

if not ok then
    return
end

vim.g.rainbow_delimiters = {
    enable = true,
    -- disable = { "jsx", "cpp" },
    extended_mode = true,
    max_file_lines = nil,
    -- colors = {},
    -- termcolors = {}
    strategy = {
        [''] = rainbow.strategy['global'],
        vim = rainbow.strategy['local'],
    },
    query = {
        [''] = 'rainbow-delimiters',
        latex = 'rainbow-blocks',
    },
    highlight = {
        'RainbowDelimiterRed',
        'RainbowDelimiterYellow',
        'RainbowDelimiterBlue',
        'RainbowDelimiterOrange',
        'RainbowDelimiterGreen',
        'RainbowDelimiterViolet',
        'RainbowDelimiterCyan',
    },
}
