--                 _ _                       --
--  _ __ ___ _ __ | | |_ ___ _ __ _ __ ___   --
-- | '__/ _ \ '_ \| | __/ _ \ '__| '_ ` _ \  --
-- | | |  __/ |_) | | ||  __/ |  | | | | | | --
-- |_|  \___| .__/|_|\__\___|_|  |_| |_| |_| --
--          |_|                              --

local ft_ok, fterm = pcall(require, 'FTerm')

if not ft_ok then
    return setmetatable({}, {
        __index = function ()
            return function () end
        end,
    })
end

---@alias replterm.Term Term
---@class replterm.Opts: Config

local M = {}

---@type table<integer,replterm.Term?>
local lookup = setmetatable({}, { __mode = 'v' })

local FT_NAME = 'fterm.lsp_repl'

---create a terminal with a repl for a language
---@param id integer lsp client id
---@param opts replterm.Opts repl options
---@return replterm.Term #repl
local repl_create = function (id, opts)
    if lookup[id] then
        return lookup[id]
    end

    ---@type replterm.Opts
    local defaults = {
        cmd = vim.o.shell,
        border = 'rounded',
        dimensions = {
            width = 0.8,
            height = 0.8,
            x = 0.5,
            y = 0.5,
        },
    }

    local cfg = vim.tbl_deep_extend('keep', opts, defaults) ---@cast cfg -nil
    cfg.ft = FT_NAME

    local repl = fterm:new(cfg)
    lookup[id] = repl

    return repl
end


M = {
    create = repl_create,
    filetype = function () return FT_NAME end,
}

return M
