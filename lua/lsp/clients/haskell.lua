--  _               _        _ _  --
-- | |__   __ _ ___| | _____| | | --
-- | '_ \ / _` / __| |/ / _ \ | | --
-- | | | | (_| \__ \   <  __/ | | --
-- |_| |_|\__,_|___/_|\_\___|_|_| --
--                                --
---@diagnostic disable: missing-fields
local util = require('lspconfig.util')

local M = {}

local cabal_files = { '*.cabal', 'cabal.project' }
local stack_files = { 'stack.yaml' }

---@param bufnr integer?
local function repl_cmd(bufnr)
    local f = vim.api.nvim_buf_get_name(bufnr or 0)

    local tools = {
        { exe = 'stack', files = stack_files, repl = 'stack ghci' },
        { exe = 'cabal', files = cabal_files, repl = 'cabal new-repl' },
    }

    for _, tool in ipairs(tools) do
        local root = util.root_pattern(unpack(tool.files))(f)
        if root and vim.fn.executable(tool.exe) ~= 0 then
            return string.format('cd %s && %s', root, tool.repl)
        end
    end

    --- Use the system shell as a fallback if ghci cannot be found
    if vim.fn.executable('ghci') == 0 then
        vim.notify_once(
            'hls: could not find any of the executables: `ghci`, `stack` or `cabal`',
            vim.log.levels.WARN
        )
        return vim.o.shell
    end

    return 'ghci ' .. f
end

local root_files = vim.iter.flatten {
    cabal_files,
    stack_files,
    { '.git', 'Makefile', 'hie.yaml', 'package.yaml' },
}

---@type lsp_client_cfg
M = {
    cmd = { 'haskell-language-server-wrapper', '--lsp' },
    filetypes = { 'haskell', 'lhaskell' },
    root_dir = util.root_pattern(unpack(root_files)),
    single_file_support = true,

    settings = {
        haskell = {
            formattingProvider = 'ormolu',
            plugin = {
                rename = {
                    config = {
                        crossModule = true,
                    },
                },
            },
        },
    },

    on_attach = function (client, bufnr)
        ---Injected properties
        client.config.__repl_opts = {
            cmd = repl_cmd(bufnr),
        }
    end,

    -- the default configuration has bad spacing and too much info
    lspinfo = function (cfg)
        local result = vim.system(
            { cfg.cmd[1], '--version' },
            { text = true }
        ):wait()

        local extra = {}

        if result.stdout then
            -- 17 is a 'hardcoded' value in lspconfig's make_client_info()
            local pfx = 'version:' .. string.rep(' ', 17 - #'version:')

            local data = vim.split(tostring(result.stdout), '\n')[1]
            extra[#extra+1] = pfx .. (data:match('(.*)%s*%(PATH') or data)
        end

        return extra
    end,
}

return M
