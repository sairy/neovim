---Formatting settings for sumneko_lua

---@see https://github.com/CppCXY/EmmyLuaCodeStyle/tree/master/docs
---@type table<string,string>
local M = {
    ---Basic options---
    indent_style = 'space',
    indent_size = '4',
    -- tab_width = '4',
    quote_style = 'single',
    call_arg_parentheses = 'remove_table_only',
    continuation_indent = '4',
    max_line_length = '90',
    -- end_of_line = 'LF',
    trailing_table_separator = 'smart',
    -- detect_end_of_line = 'true',
    insert_final_newline = 'false',

    ---Whitespace option---
    space_around_table_field_list = 'true',

    -- Lua>= 5.4 only
    space_before_attribute = 'false',

    -- local function f() end
    space_before_function_open_parenthesis = 'false',
    -- f()
    space_before_function_call_open_parenthesis = 'false',
    -- local f = function () end
    space_before_closure_open_parenthesis = 'true',
    -- ipairs { 'a', 'b', 'c' }
    space_before_function_call_single_arg = 'true',

    space_before_open_square_bracket = 'false',
    space_inside_function_call_parentheses = 'false',
    space_inside_function_param_list_parentheses = 'false',
    space_inside_square_brackets = 'false',

    -- t[#t+1]; misleading name
    space_around_table_append_operator = 'true',
    ignore_spaces_inside_function_call = 'false',
    space_before_inline_comment = '1',

    ---Operator blank---
    space_around_math_operator = 'true',
    space_after_comma = 'true',
    space_after_comma_in_for_statement = 'true',
    space_around_concat_operator = 'true',

    ---Alignment---
    align_call_args = 'true',
    align_function_params = 'true',
    align_continuous_assign_statement = 'false',
    align_continuous_rect_table_field = 'false',
    align_if_branch = 'false',
    align_array_table = 'true',

    ---Other indentation settings---
    never_indent_before_if_condition = 'false',
    -- never_indent_comment_on_if_branch = 'false',

    ---Line layout---
    line_space_after_if_statement = 'keep',
    line_space_after_do_statement = 'keep',
    line_space_after_while_statement = 'keep',
    line_space_after_repeat_statement = 'keep',
    line_space_after_for_statement = 'keep',
    line_space_after_local_or_assign_statement = 'keep',
    line_space_after_function_statement = 'fixed(2)',
    line_space_after_expression_statement = 'keep',
    line_space_after_comment = 'keep',

    ---Line break---
    break_all_list_when_line_exceed = 'true',
    -- auto_collapse_lines = 'false',

    ---Preferences---
    -- ignore_space_after_colon = 'false',
    -- remove_call_expression_list_finish_comma = 'true',
}

return M
