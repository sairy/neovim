--    _                                _       _    --
--   (_) __ ___   ____ _ ___  ___ _ __(_)_ __ | |_  --
--   | |/ _` \ \ / / _` / __|/ __| '__| | '_ \| __| --
--   | | (_| |\ V / (_| \__ \ (__| |  | | |_) | |_  --
--  _/ |\__,_| \_/ \__,_|___/\___|_|  |_| .__/ \__| --
-- |__/                                 |_|         --
---@diagnostic disable: missing-fields
local util = require('lspconfig.util')

---@type lsp_client_cfg
local M = {
    cmd = { 'typescript-language-server', '--stdio' },
    filetypes = {
        'javascript',
        'javascript.jsx',
        'javascriptreact',
        'typescript',
        'typescript.tsx',
        'typescriptreact',
    },

    root_dir = util.root_pattern('.git', 'package.json', 'tsconfig.json', 'jsconfig.json'),
    single_file_support = true,

    init_options = {
        hostInfo = 'neovim',
    },

    settings = {
        completions = {
            completeFunctionCalls = true,
        },
    },

    on_attach = function (client)
        if vim.fn.executable('node') == 1 then
            client.config.__repl_opts = { cmd = 'node' }
        end
    end,
}

return M
