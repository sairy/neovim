--              _   _                  --
--  _ __  _   _| |_| |__   ___  _ __   --
-- | '_ \| | | | __| '_ \ / _ \| '_ \  --
-- | |_) | |_| | |_| | | | (_) | | | | --
-- | .__/ \__, |\__|_| |_|\___/|_| |_| --
-- |_|    |___/                        --
---@diagnostic disable: missing-fields
local util = require('lspconfig.util')

---@type lsp_client_cfg
local M = {
    cmd = { 'pylsp' },
    filetypes = { 'python' },

    root_dir = util.root_pattern(
        '.git',
        'pyproject.toml',
        'setup.py',
        'setup.cfg',
        'requirements.txt',
        'Pipfile'
    ),
    single_file_support = true,

    settings = {
        pylsp = {
            plugins = {
                -- formatter options
                black = { enabled = true },
                autopep8 = { enabled = false },
                yapf = { enabled = false },
                -- linter options
                pylint = { enabled = true, executable = 'pylint' },
                pyflakes = { enabled = false },
                pycodestyle = { enabled = false },
                -- type checker
                pylsp_mypy = {
                    enabled = true,
                    report_progress = true,
                    live_mode = false,
                },
                -- auto-completion options
                jedi_completion = {
                    enabled = true,
                    fuzzy = true,
                    include_params = true,
                    include_class_objects = true,
                },
                -- import sorting
                pyls_isort = { enabled = true },
            },
        },
    },
    flags = {
        debounce_text_changes = 200,
    },
    on_init = function (client)
        if vim.env['VIRTUAL_ENV'] then
            local mypy = client.config.settings.pylsp.plugins.pylsp_mypy
            mypy = vim.tbl_deep_extend('force', mypy, {
                overrides = {
                    '--python-executable',
                    vim.env['VIRTUAL_ENV'] .. '/bin/python',
                    true,
                },
            })

            client.config.settings.pylsp.plugins.pylsp_mypy = mypy
        end
    end,
}

M.__repl_opts = {
    cmd = 'python',
}

return M
