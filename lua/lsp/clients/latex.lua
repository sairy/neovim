--  _       _             --
-- | | __ _| |_ _____  __ --
-- | |/ _` | __/ _ \ \/ / --
-- | | (_| | ||  __/>  <  --
-- |_|\__,_|\__\___/_/\_\ --
--                        --
---@diagnostic disable: missing-fields
local util = require('lspconfig.util')

---@type lsp_client_cfg
local M = {
    cmd = { 'texlab' },
    filetypes = { 'tex', 'latex', 'bib' },

    root_dir = util.root_pattern('.git', 'Makefile'),

    settings = {
        texlab = {
            auxDirectory = '.', -- cwd
            bibtexFormatter = 'texlab',
            build = {
                executable = 'make',
                onSave = true,
            },
            chktex = {
                onEdit = true,
                onOpenAndSave = true,
            },
            diagonsticsDelay = 300,
            formatterLineLength = 80,
            forwardSearch = { args = {} },
            latexFormatter = 'latexindent',
            latexindent = {
                modifyLinebreaks = false,
            },
        },
    },
}

return M
