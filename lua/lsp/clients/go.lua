--               --
--   __ _  ___   --
--  / _` |/ _ \  --
-- | (_| | (_) | --
--  \__, |\___/  --
--  |___/        --
---@diagnostic disable: missing-fields
local util = require('lspconfig.util')

---@type lsp_client_cfg
local M = {
    cmd = { 'gopls' },
    filetypes = { 'go', 'gomod', 'gowork', 'gotmpl' },
    root_dir = util.root_pattern('go.work', 'go.mod', '.git'),
    single_file_support = true,
    settings = {
        gopls = {
            analyses = {
                ST1003 = true,
                fieldalignment = false,
                fillreturns = true,
                nilness = true,
                nonewvars = true,
                shadow = true,
                undeclaredname = true,
                unreachable = true,
                unusedparams = true,
                unusedwrite = true,
                useany = true,
            },
            codelenses = {
                gc_details = true, -- Show a code lens toggling the display of gc's choices.
                generate = true,   -- show the `go generate` lens.
                regenerate_cgo = true,
                test = true,
                tidy = true,
                upgrade_dependency = true,
                vendor = true,
            },
            usePlaceholders = true,
            completeUnimported = true,
            semanticTokens = true,
            staticcheck = true,
        },
    },
}

M.__repl_opts = {
    cmd = 'yaegi',
}

return M
