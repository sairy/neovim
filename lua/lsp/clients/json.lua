--    _                  --
--   (_)___  ___  _ __   --
--   | / __|/ _ \| '_ \  --
--   | \__ \ (_) | | | | --
--  _/ |___/\___/|_| |_| --
-- |__/                  --
---@diagnostic disable: missing-fields
local util = require('lspconfig.util')

---@type lsp_client_cfg
local M = {
    cmd = { 'vscode-json-language-server', '--stdio' },
    filetypes = { 'json', 'jsonc' },

    root_dir = util.root_pattern('.git'),
    single_file_support = true,

    init_options = {
        provideFormatter = true,
    },
}

return M
