--       _                    --
--   ___| | __ _ _ __   __ _  --
--  / __| |/ _` | '_ \ / _` | --
-- | (__| | (_| | | | | (_| | --
--  \___|_|\__,_|_| |_|\__, | --
--                     |___/  --
---@diagnostic disable: missing-fields
local util = require('lspconfig.util')

---@type lsp_client_cfg
local M = {
    cmd = {
        'clangd',
        '--enable-config',
        '--background-index',
        '--clang-tidy',
        '--all-scopes-completion',
        '--completion-style=detailed',
        '--function-arg-placeholders',
        '--header-insertion=iwyu',
        '--header-insertion-decorators',
        '--include-cleaner-stdlib',
        '--limit-references=3000',
        '--limit-results=350',
    },
    filetypes = { 'c', 'cpp', 'objc', 'objcpp', 'cuda', 'proto' },
    root_dir = util.root_pattern('.git', 'meson.build', 'Makefile'),
    single_file_support = true,
    on_attach = function ()
        local map = require('util.common').bind
        local cmds = require('lspconfig.configs.clangd').commands

        map('n', '<leader>h', cmds.ClangdSwitchSourceHeader[1], { silent = true })
    end,
}

return M
