--                _ _        --
--  _____   _____| | |_ ___  --
-- / __\ \ / / _ \ | __/ _ \ --
-- \__ \\ V /  __/ | ||  __/ --
-- |___/ \_/ \___|_|\__\___| --
--                           --
---@diagnostic disable: missing-fields
local util = require("lspconfig.util")

---@type lsp_client_cfg
local M = {
    cmd = { 'svelteserver', '--stdio' },
    filetypes = { 'svelte' },
    root_dir = util.root_pattern('.git', 'package.json')
}

return M
