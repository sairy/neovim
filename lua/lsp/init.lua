--  _                              __ _        --
-- | |___ _ __     ___ ___  _ __  / _(_) __ _  --
-- | / __| '_ \   / __/ _ \| '_ \| |_| |/ _` | --
-- | \__ \ |_) | | (_| (_) | | | |  _| | (_| | --
-- |_|___/ .__/   \___\___/|_| |_|_| |_|\__, | --
--       |_|                            |___/  --

-- avoid loading lsp if running in headless mode
if #vim.api.nvim_list_uis() == 0 then
    return
end

local lsp_ok, lsp = pcall(require, 'lspconfig')
if not lsp_ok then
    return
end

---@alias client_t table<string, string>

---@type client_t
local clients = {
    clangd      = 'clang',
    gopls       = 'go',
    hls         = 'haskell',
    jsonls      = 'json',
    lua_ls      = 'sumneko',
    pylsp       = 'python',
    svelte      = 'svelte',
    texlab      = 'latex',
    ts_ls       = 'js',
}

---update client capabilities
---@param old? lsp.ClientCapabilities
---@return lsp.ClientCapabilities
local function renew_capabilities(old)
    local new = vim.lsp.protocol.make_client_capabilities()

    -- force snippet support due to cmp
    ---@diagnostic disable-next-line: inject-field
    new.textDocument.completion.completionItem.snippetSupport = true

    local ok, cmp_lsp = pcall(require, 'cmp_nvim_lsp')
    if ok then
        new = vim.tbl_deep_extend(
            'force',
            new,
            cmp_lsp.default_capabilities(),
            old or {}
        ) --[[@as lsp.ClientCapabilities]]
    end

    return new
end

---function to load all clients
---@param client_t client_t
local function loadlsp(client_t)
    local pfx = 'lsp.clients.'

    for client, file in pairs(client_t) do
        ---@type boolean, lsp_client_cfg
        local ok, cfg = pcall(require, pfx .. file)

        if ok then
            cfg.capabilities = renew_capabilities(cfg.capabilities)
            cfg.on_attach = require('lsp.attach').on_attach(cfg.on_attach)

            lsp[client].setup(cfg)
        end
    end
end

-- rounded wins
require('lspconfig.ui.windows').default_options.border = 'rounded'

local _open_floating_preview = vim.lsp.util.open_floating_preview
function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
    local borderchars = { '╭', '─', '╮', '│', '╯', '─', '╰', '│' }

    opts = vim.tbl_extend('force', opts or {}, {
        border = borderchars,
        max_width = math.floor(vim.fn.winwidth(0) / 2),
        max_height = math.floor(vim.fn.winheight(0) / 2),
    })

    return _open_floating_preview(contents, syntax, opts, ...)
end

loadlsp(clients)

--HACK: get lsp server to startup on empty buffers
--while also having the plugin be lazy loaded
vim.bo.filetype = vim.bo.filetype

-- EOF
