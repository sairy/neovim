--                    _       --
--  _ __   __ ___   _(_) ___  --
-- | '_ \ / _` \ \ / / |/ __| --
-- | | | | (_| |\ V /| | (__  --
-- |_| |_|\__,_| \_/ |_|\___| --
--                            --

local ok, navic = pcall(require, 'nvim-navic')

if not ok then
    return
end

local util = require('util.common')
local icons = require('kind').icons

navic.setup {
    icons = icons,
    highlight = true,
    separator = ' > ',
    depth_limit = 0,
    depth_limit_indicator = '..',
}

---NOTE: This allows for the plugin to be lazy loaded
-- while still attaching navic to the lsp server(s)
util.aucmd('LspAttach', {
    pattern = '*',
    callback = function (args)
        local nv = require('nvim-navic')

        local clients = vim.lsp.get_clients {
            bufnr = args.buf,
            method = 'documentSymbolProvider',
        }

        for _, cl in ipairs(clients) do
            if cl.name ~= 'null-ls' then
                nv.attach(cl, args.buf)
            end
        end
    end,
    group = util.augroup('nvim-navic'),
})
