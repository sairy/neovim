--  _                _                           --
-- | |__   __ _ _ __| |__   ___  ___ _   _  ___  --
-- | '_ \ / _` | '__| '_ \ / _ \/ __| | | |/ _ \ --
-- | |_) | (_| | |  | |_) |  __/ (__| |_| |  __/ --
-- |_.__/ \__,_|_|  |_.__/ \___|\___|\__,_|\___| --
--                                               --
local ok, barbecue = pcall(require, 'barbecue')

if not ok then
    return
end

local icons = require('kind').icons

-- Theme override
local theme = require('barbecue.theme.catppuccin')
do
    local C = require('catppuccin.palettes').get_palette() --[[@as CtpColors<string>]]

    for _, hl in ipairs { 'ellipsis', 'separator', 'modified' } do
        theme[hl].fg = C.subtext1
    end
end

local symbols = {
    empty_set = '∅',
    ellipsis = '…',
    nice_arrow = '❯',
    reverse_nice_arrow = '❮',
}

barbecue.setup {
    show_navic = true,
    attach_navic = false,

    create_autocmd = true,

    -- include_buftypes = { '' },

    exclude_filetypes = {
        'Fterm',
        'alpha',
        'fidget',
        'gitcommit',
        'gitrebase',
        'man',
    },

    modifiers = {
        dirname = ':~:.',
        basename = '',
    },

    custom_section = nil,

    theme = theme,

    context_follow_icon_color = false,

    show_basename = true,
    show_dirname = true,
    show_modified = false,

    symbols = {
        modified = '●',
        ellipsis = symbols.ellipsis,
        separator = '',
    },

    kinds = icons,
}

--[=[
do
    local util = require('util.common')

    vim.cmd('Barbecue toggle')

    local g = util.augroup('winbar')
    util.aucmd('LspAttach', {
        pattern = '*',
        command = 'Barbecue show',
        group = g,
    })

    util.aucmd('LspDetach', {
        pattern = '*',
        command = 'Barbecue hide',
        group = g,
    })

    util.aucmd('BufEnter', {
        pattern = '*',
        callback = function (args)
            local clients = vim.lsp.get_clients { bufnr = args.buf }

            return require('barbecue.ui').toggle(vim.tbl_count(clients) ~= 0)
        end,
        group = g,
    })
end
--]=]
