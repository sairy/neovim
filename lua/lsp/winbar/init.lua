--           _       _                 --
-- __      _(_)_ __ | |__   __ _ _ __  --
-- \ \ /\ / / | '_ \| '_ \ / _` | '__| --
--  \ V  V /| | | | | |_) | (_| | |    --
--   \_/\_/ |_|_| |_|_.__/ \__,_|_|    --
--                                     --
---Which winbar handler to use
-- ---@type 'barbecue'
-- local handler = 'barbecue'

-- pcall(require, 'lsp.winbar.' .. handler)

return pcall(require, 'lsp.winbar.barbecue')
