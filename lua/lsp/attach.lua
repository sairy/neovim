local M = {}

---@param old_attach? fun(client: lsp_client, bufnr: integer)
M.on_attach = function (old_attach)
    ---@param c lsp_client
    ---@param bufnr integer
    return function (c, bufnr)
        if type(old_attach) == 'function' then
            old_attach(c, bufnr)
        end

        require('lsp.mappings').on_attach(bufnr)

        if c:supports_method('textDocument/inlayHint') then
            vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
        end

        ---REPLterm
        if c.config.__repl_opts then
            local repl = require('lsp.replterm')
            local utils = require('util.common')

            local term = repl.create(c.id, c.config.__repl_opts)

            --- buffer local because one buffer will have one replterm only
            utils.bufbind(bufnr, 'n', '<C-b>', function ()
                term:toggle()
            end)

            --- term.buf is volatile so it can't be used for a buflocal bind
            --- instead we create a binding global to 't'
            --- that closes any win containing a replterm
            utils.bind('t', '<C-b>', function ()
                local buf = vim.api.nvim_get_current_buf()
                if vim.bo[buf].filetype == repl.filetype() then
                    vim.cmd.close()
                end
            end)
        end
    end
end

return M
