--                              _                  --
--  _ __ ___   __ _ _ __  _ __ (_)_ __   __ _ ___  --
-- | '_ ` _ \ / _` | '_ \| '_ \| | '_ \ / _` / __| --
-- | | | | | | (_| | |_) | |_) | | | | | (_| \__ \ --
-- |_| |_| |_|\__,_| .__/| .__/|_|_| |_|\__, |___/ --
--                 |_|   |_|            |___/      --

local M = {}

---@param bufnr integer
local on_attach = function (bufnr)
    ---Revert https://github.com/neovim/neovim/commit/33e1a8cd7042816a064c0d2bf32b6570d7e88b79
    if vim.fn.maparg('K', 'n', false, true).callback == vim.lsp.buf.hover then
        vim.keymap.del('n', 'K', { buffer = bufnr })
    end

    local picker = require('util.picker')
    local bufbind = require('util.common').bufbind

    ---@param mode keymap_mode|keymap_mode[]
    ---@param shortcut string
    ---@param action string|fun()
    local map = function (mode, shortcut, action)
        local opts = { noremap = true, silent = true }
        return bufbind(bufnr, mode, shortcut, action, opts)
    end

    local extra = picker {
        prompt = 'LSP extra actions',
        items = {
            {
                name = ' Goto Declaration',
                action = vim.lsp.buf.declaration,
            },
            {
                name = ' Goto Type Definition',
                action = vim.lsp.buf.type_definition,
            },
            {
                name = ' Incoming Calls',
                action = vim.lsp.buf.incoming_calls,
            },
            {
                name = '󰉺 View Implementations',
                action = vim.lsp.buf.implementation,
            },
            {
                name = '⮌ View References',
                action = vim.lsp.buf.references,
            },
            {
                name = '✚ Diagnostics to qflist',
                action = vim.diagnostic.setqflist,
            },
            {
                name = '󰡱 Toggle Inlay-Hints',
                action = function ()
                    local filter = { bufnr = bufnr }
                    if vim.lsp.inlay_hint.is_enabled(filter) then
                        vim.lsp.inlay_hint.enable(false, filter)
                    else
                        vim.lsp.inlay_hint.enable(true, filter)
                    end
                end,
            },
        },
        config = {
            telescope = {
                theme = 'dropdown',
            },
        },
    }

    map('n', '<leader>lr', vim.lsp.buf.rename)
    map('n', '<leader>lh', vim.lsp.buf.hover)
    map('n', '<leader>ld', vim.lsp.buf.definition)
    map({ 'n', 'v' }, '<leader>la', vim.lsp.buf.code_action)
    map({ 'n', 'v' }, '<leader>lf', function ()
        vim.lsp.buf.format { bufnr = bufnr, async = true }
    end)

    map('n', '<leader>le', extra)
end


M = {
    on_attach = on_attach,
}

return M
