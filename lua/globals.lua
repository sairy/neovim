--        _       _           _      --
--   __ _| | ___ | |__   __ _| |___  --
--  / _` | |/ _ \| '_ \ / _` | / __| --
-- | (_| | | (_) | |_) | (_| | \__ \ --
--  \__, |_|\___/|_.__/ \__,_|_|___/ --
--  |___/                            --

---`vim.notify` wrapped in `vim.schedule` for async operations
---@type fun(msg:string, level:string, opts:table?)
vim.notify_schedule = vim.schedule_wrap(function (...)
    vim.notify(...)
end)

---@generic T, U
---@param fn fun(t: T): U
---@param tbl table<T>
---@return table<U>
---@diagnostic disable-next-line: inject-field
vim.iter.map = function(fn, tbl)
    return vim.iter(tbl):map(fn):totable()
end

---@param tbl table
---@param depth? number
---@return table
---@diagnostic disable-next-line: inject-field
vim.iter.flatten = function(tbl, depth)
    return vim.iter(tbl):flatten(depth):totable()
end
