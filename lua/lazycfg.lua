--  _                  --
-- | | __ _ _____   _  --
-- | |/ _` |_  / | | | --
-- | | (_| |/ /| |_| | --
-- |_|\__,_/___|\__, | --
--              |___/  --
local config = vim.fn.stdpath('config')

local plugdir = config .. '/plugins/lazy'
local lazypath = plugdir .. '/lazy.nvim'

_G.Lazy_bootstrap = false
if not vim.uv.fs_stat(lazypath) then
    _G.Lazy_bootstrap = true

    vim.notify(
        string.format('Installing lazy.nvim to %s ...', lazypath),
        vim.log.levels.INFO
    )

    vim.fn.system {
        'git',
        'clone',
        '--filter=blob:none',
        'https://github.com/folke/lazy.nvim.git',
        lazypath,
    }

    if vim.v.shell_error ~= 0 then
        vim.notify(
            'error: something went wrong during installation!',
            vim.log.levels.ERROR
        )
        return
    end

    vim.notify(
        'Installation completed with success!!',
        vim.log.levels.INFO
    )
end

vim.opt.runtimepath:prepend(lazypath)

---@type LazyConfig
local opts = {
    root = plugdir,
    defaults = {
        lazy = true,
        version = nil,
    },

    lockfile = config .. '/lazylock.json',
    concurrency = tonumber(vim.fn.system('nproc')) or 1,

    git = {
        log = { '--since=3 days ago' },
        timeout = 120,
        url_format = 'https://github.com/%s.git',
    },

    rocks = {
        root = vim.fn.stdpath('data') .. '/lazy-rocks',
        server = 'https://nvim-neorocks.github.io/rocks-binaries/',
    },

    dev = nil,

    install = {
        missing = true,
        colorscheme = { 'catppuccin-mocha', 'catppuccin', 'rose-pine', 'habamax' },
    },

    ui = {
        size = { width = 0.8, height = 0.8 },
        wrap = true,
        border = 'rounded',
        pills = true,
        icons = {
            cmd = ' ',
            config = '',
            event = ' ',
            favorite = ' ',
            ft = ' ',
            init = ' ',
            import = ' ',
            keys = ' ',
            lazy = '󰒲 ',
            loaded = '●',
            not_loaded = '○',
            plugin = ' ',
            runtime = ' ',
            require = '󰢱 ',
            source = ' ',
            start = ' ',
            task = '✔ ',
            list = { '●', '➜', '★', '‒' },
        },

        browser = vim.env.BROWSER or 'xdg-open',
        throttle = 20,

        custom_keys = {
            ['<localleader>l'] = false,
            ['<localleader>t'] = false,
        },
    },

    diff = {
        cmd = 'git',
    },

    checker = {
        enabled = false, -- don't check for plugin updates
        concurrency = nil,
        notify = false,
        frequency = nil,
        check_pinned = true,
    },

    change_detection = {
        enabled = true,
        notify = true,
    },

    performance = {
        cache = { enabled = true },
        reset_packpath = false,

        rtp = {
            reset = false,
            paths = {},
            disabled_plugins = {
                'fzf',
                'lf',
                'netrwPlugin',
            },
        },
    },

    readme = {
        enabled = false,
        -- root = nil,
        -- files = {},
        -- skip_if_doc_exists = true,
    },

    state = vim.fn.stdpath('state') .. '/lazy/state.json',
    build = {
        warn_on_override = false,
    },

    profiling = {
        loader = false,
        require = false,
    }
}

if vim.env.NVIM_LAZY_DEBUG then
    opts.profiling.loader = true
    opts.profiling.require = true
end

local ok, lazy = pcall(require, 'lazy')

if ok then
    lazy.setup('plugins', opts)

    local bind = require('util.common').bind

    bind('n', '<leader>pu', lazy.sync)
    bind('n', '<leader>ps', lazy.show)

    if Lazy_bootstrap and vim.fn.exists(':Alpha') then
        vim.cmd([[
            " close Lazy's window & run our dashboard
            close
            lua require('alpha') -- force plugin load
            Alpha
        ]])
    end
end
