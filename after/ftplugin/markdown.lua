vim.opt_local.spell = true
vim.opt_local.conceallevel = 2

vim.opt_local.foldmethod = 'expr'
vim.opt_local.foldexpr = 'nvim_treesitter#foldexpr()'
vim.opt_local.foldenable = false
