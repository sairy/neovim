; extends

((function_call
   name: (_) @_osexec_ident
   arguments: (arguments . (string content: _ @injection.content)))
 (#set! injection.language "bash")
 (#eq? @_osexec_ident "os.execute"))
